// Given the root of a binary tree, return the number of nodes where the value of the node is equal to the average of the values in its subtree.
//
// Note:
//
// The average of n elements is the sum of the n elements divided by n and rounded down to the nearest integer.
// A subtree of root is a tree consisting of root and all of its descendants.

package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

var counting int

func traverse(node *TreeNode) (sum int, count int) {
	if node != nil {
		sL, cL := traverse(node.Left)
		sR, cR := traverse(node.Right)
		s := sL + sR + node.Val
		c := cL + cR + 1
		if int(s/c) == node.Val {
			counting++
		}
		return s, c
	}
	return 0, 0
}

func averageOfSubtree(root *TreeNode) int {
	counting = 0
	_, _ = traverse(root)
	return counting
}

func main() {
	var root *TreeNode = &TreeNode{Val: 4}
	root.Left = &TreeNode{Val: 8}
	root.Left.Left = &TreeNode{Val: 0}
	root.Left.Right = &TreeNode{Val: 1}
	root.Right = &TreeNode{Val: 5}
	root.Right.Right = &TreeNode{Val: 6}

	fmt.Println(averageOfSubtree(root))
}
