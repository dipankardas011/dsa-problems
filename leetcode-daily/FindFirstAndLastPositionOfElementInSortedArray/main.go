// Given an array of integers nums sorted in non-decreasing order, find the starting and ending position of a given target value.
//
// If target is not found in the array, return [-1, -1].
//
// You must write an algorithm with O(log n) runtime complexity.

package main

import "fmt"

type searchPosition int

const (
	Left  = searchPosition(0)
	Right = searchPosition(1)
)

func searchRange(nums []int, target int) []int {
	ans := make([]int, 2)
	ans[0] = -1
	ans[1] = -1

	ans[0] = binarySearch(nums, target, Left)
	ans[1] = binarySearch(nums, target, Right)

	return ans
}

func binarySearch(arr []int, target int, pos searchPosition) int {
	start := 0
	end := len(arr) - 1
	finalPos := -1

	for start <= end {
		mid := start + (end-start)/2
		if arr[mid] == target {
			finalPos = mid
			if pos == Left {
				end = mid - 1
			} else {
				start = mid + 1
			}
		} else if arr[mid] < target {
			start = mid + 1
		} else {
			end = mid - 1
		}
	}
	return finalPos
}

func main() {
	fmt.Println(searchRange([]int{1, 2, 2, 3}, 2))
}
