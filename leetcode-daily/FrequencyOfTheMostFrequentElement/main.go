// The frequency of an element is the number of times it occurs in an array.
//
// You are given an integer array nums and an integer k. In one operation, you can choose an index of nums and increment the element at that index by 1.
//
// Return the maximum possible frequency of an element after performing at most k operations.

package main

import (
	"fmt"
	"sort"
)

func maxFrequency(nums []int, k int) int {
	sort.Ints(nums)
	left, ans, currSum := 0, 0, 0

	for right := 0; right < len(nums); right++ {
		target := nums[right]
		currSum += target
		for k < (right-left+1)*target-currSum {
			currSum -= nums[left]
			left++
		}
		ans = max(ans, right-left+1)
	}

	return ans
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func main() {
	fmt.Println(maxFrequency([]int{1, 2, 4}, 5))
}
