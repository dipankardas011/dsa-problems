package main

import "fmt"

var (
	dirs = [][]int{{0, -1}, {0, 1}, {1, 0}, {-1, 0}}
)

func dfs(mat [][]int, row, col int) bool {
	rowLen := len(mat)
	colLen := len(mat[0])

	if row < 0 || row >= rowLen ||
		col < 0 || col >= colLen ||
		mat[row][col] != 0 {
		return false
	}

	if row == rowLen-1 {
		return true
	}
	mat[row][col] = -1
	for _, dir := range dirs {
		i := dir[0] + row
		j := dir[1] + col

		if dfs(mat, i, j) {
			return true
		}
	}
	return false
}

func fillMat(mat [][]int, cells [][]int, day int) [][]int {
	rLen := len(mat)
	cLen := len(mat[0])

	for i := 0; i < rLen; i++ {
		for j := 0; j < cLen; j++ {
			mat[i][j] = 0
		}
	}

	for water := 0; water < day; water++ {
		d := cells[water]
		mat[d[0]-1][d[1]-1] = 1
	}

	return mat
}

func handler(mat [][]int, cells [][]int, day int) bool {
	mat = fillMat(mat, cells, day)

	cL := len(mat[0])

	// as the starting point can be any col of first row
	for col := 0; col < cL; col++ {
		if mat[0][col] == 0 && dfs(mat, 0, col) {
			return true
		}
	}
	return false

}

func latestDayToCross(row int, col int, cells [][]int) int {
	mat := make([][]int, row)
	for i := 0; i < row; i++ {
		mat[i] = make([]int, col)
	}

	daysLeft := 0
	daysRight := len(cells) - 1
	storeLastSuccess := 0
	for daysLeft <= daysRight {
		day := daysLeft + (daysRight-daysLeft)/2

		if handler(mat, cells, day) {
			storeLastSuccess = day
			daysLeft = day + 1
		} else {
			// false means it should be left
			daysRight = day - 1
		}
	}

	return storeLastSuccess
}
func main() {
	fmt.Println(latestDayToCross(2, 2, [][]int{{1, 1}, {2, 1}, {1, 2}, {2, 2}}))
}
