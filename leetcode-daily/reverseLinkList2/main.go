package main

func reverseBetween(head *ListNode, left int, right int) *ListNode {
	var prevStart *ListNode
	var start *ListNode = head
	for i := 1; start != nil && i < left; i++ {
		prevStart = start
		start = start.Next
	}

	var prev *ListNode = nil
	var curr *ListNode = start
	var next *ListNode = nil

	for i := left; curr != nil && i <= right; i++ {
		next = curr.Next
		curr.Next = prev
		prev = curr

		curr = next
	}

	// entire ll reverse
	if curr == nil && prevStart == nil {
		head = prev
		return head
	}

	// from head to node which is not end
	if curr != nil && prevStart == nil {
		head = prev

		start.Next = next
		return head
	}

	// from the node to end
	if curr == nil && prevStart != nil {
		prevStart.Next = prev
		return head
	}

	// from neither of the extreme ends
	prevStart.Next = prev
	start.Next = next
	return head
}
