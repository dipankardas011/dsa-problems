// There are n items each belonging to zero or one of m groups where group[i] is the group that the i-th item belongs to and it's equal to -1 if the i-th item belongs to no group. The items and the groups are zero indexed. A group can have no item belonging to it.
//
// Return a sorted list of the items such that:
//
// The items that belong to the same group are next to each other in the sorted list.
// There are some relations between these items where beforeItems[i] is a list containing all the items that should come before the i-th item in the sorted array (to the left of the i-th item).
// Return any solution if there is more than one solution and return an empty list if there is no solution.

package main

func sortItems(n int, m int, group []int, beforeItems [][]int) []int {
	itemCount := n
	groupCount := m

	for i, grp := range group {
		if grp == -1 {
			group[i] = groupCount
			groupCount++
		}
	}

	itemGraph := make(map[int][]int, itemCount)
	groupGraph := make(map[int][]int, groupCount)

	itemIndegree := make([]int, itemCount)
	groupIndegree := make([]int, groupCount)

	for item := 0; item < itemCount; item++ {
		itemGraph[item] = []int{}
		itemIndegree[item] = 0
	}

	for grp := 0; grp < groupCount; grp++ {
		groupGraph[grp] = []int{}
		groupIndegree[grp] = 0
	}

	for item := 0; item < itemCount; item++ {
		for _, dependency := range beforeItems[item] {
			itemIndegree[item]++
			itemGraph[dependency] = append(itemGraph[dependency], item)

			if group[item] != group[dependency] {
				groupIndegree[group[item]]++
				groupGraph[group[dependency]] = append(groupGraph[group[dependency]], group[item])
			}
		}
	}

	var itemSort []int = topolSort(itemGraph, itemIndegree)
	var groupSort []int = topolSort(groupGraph, groupIndegree)

	if itemSort == nil || groupSort == nil {
		return nil
	}

	orderedGrp := make(map[int][]int)

	for _, item := range itemSort {
		orderedGrp[group[item]] = append(orderedGrp[group[item]], item)
	}

	var answer []int
	for _, grp := range groupSort {
		answer = append(answer, orderedGrp[grp]...)
	}

	return answer
}

func topolSort(graph map[int][]int, indegree []int) []int {
	stk := &Stack{}
	visited := []int{}

	for vertex, _ := range graph {
		if indegree[vertex] == 0 {
			stk.Push(vertex)
		}
	}

	for stk.Size() > 0 {
		curr := stk.Pop()
		visited = append(visited, curr)

		for _, val := range graph[curr] {
			indegree[val]--
			if indegree[val] == 0 {
				stk.Push(val)
			}
		}
	}

	if len(visited) == len(graph) {
		return visited
	}

	return nil
}

type Stack struct {
	data []int
}

func (stk *Stack) Size() int {
	return len(stk.data)
}

func (stk *Stack) Push(val int) {
	stk.data = append(stk.data, val)
}

func (stk *Stack) Pop() int {
	ret := stk.data[len(stk.data)-1]

	stk.data = stk.data[:len(stk.data)-1]

	return ret
}
