// You have n dice, and each die has k faces numbered from 1 to k.
//
// Given three integers n, k, and target, return the number of possible ways (out of the kn total ways) to roll the dice, so the sum of the face-up numbers equals target. Since the answer may be too large, return it modulo 109 + 7.
package main

import "log"

// In other words, what is dp(5, 6, 18)?

// At first glance, this is seems difficult and overwhelming. But if we make one simple observation, we can reduce this big problem into several smaller sub-problems. We have 5 dice, but let's first just look at ONE of these dice (say the last one). This die can take on f=6 different values (1 to 6), so we consider what happens when we fix its value to each possibility (6 cases):

// Case 1: The last die is a 1. The remaining 4 dice must sum to 18-1=17. This can happen dp(4, 6, 17) ways.
// Case 2: The last die is a 2. The remaining 4 dice must sum to 18-2=16. This can happen dp(4, 6, 16) ways.
// Case 3: The last die is a 3. The remaining 4 dice must sum to 18-3=15. This can happen dp(4, 6, 15) ways.
// Case 4: The last die is a 4. The remaining 4 dice must sum to 18-4=14. This can happen dp(4, 6, 14) ways.
// Case 5: The last die is a 5. The remaining 4 dice must sum to 18-5=13. This can happen dp(4, 6, 13) ways.
// Case 6: The last die is a 6. The remaining 4 dice must sum to 18-6=12. This can happen dp(4, 6, 12) ways.

// dp(5, 6, 18) = dp(4, 6, 17) + dp(4, 6, 16) + dp(4, 6, 15) + dp(4, 6, 14) + dp(4, 6, 13) + dp(4, 6, 12)

const (
	MOD = 1e9 + 7
)

func helper(mem [][]int, noOfLeftDice int, k int, remTarget int) int {
	if remTarget == 0 && noOfLeftDice == 0 {
		return 1
	}
	if noOfLeftDice == 0 || remTarget <= 0 {
		return 0
	}
	if mem[noOfLeftDice][remTarget] != -1 {
		return mem[noOfLeftDice][remTarget]
	}

	ways := 0
	for face := 1; face <= k; face++ {
		ways = (ways + helper(
			mem,
			noOfLeftDice-1,
			k,
			remTarget-face,
		)) % MOD
	}

	mem[noOfLeftDice][remTarget] = ways % MOD
	return mem[noOfLeftDice][remTarget]
}

func numRollsToTarget(n int, k int, target int) int {
	mem := make([][]int, n+1)
	for i := 0; i < n+1; i++ {
		mem[i] = make([]int, target+1)
	}

	for i := 0; i < n+1; i++ {
		for j := 0; j < target+1; j++ {
			mem[i][j] = -1
		}
	}
	return helper(mem, n, k, target)
}

func main() {
	log.Println(numRollsToTarget(30, 30, 500))
}
