// You are given an array of strings words and a string chars.
//
// A string is good if it can be formed by characters from chars (each character can only be used once).
//
// Return the sum of lengths of all good strings in words.
package main

func countCharacters(words []string, chars string) int {
	memForChars := make([]int, 26)
	for _, char := range chars {
		memForChars[int(char-'a')]++
	}

	ans := 0

	for _, word := range words {
		mem := make([]int, len(memForChars))
		copy(mem, memForChars)

		flag := false
		for _, ch := range word {
			if mem[ch-'a'] > 0 {
				mem[ch-'a']--
			} else {
				flag = true
			}
		}
		if !flag {
			ans += len(word)
		}
	}

	return ans
}
