// Consider all the leaves of a binary tree, from left to right order, the values of those leaves form a leaf value sequence.
package main

import "reflect"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func leafSimilar(root1 *TreeNode, root2 *TreeNode) bool {

	var traversal func(*TreeNode, *[]int)

	traversal = func(root *TreeNode, counter *[]int) {
		if root != nil {
			traversal(root.Left, counter)
			if root.Left == nil && root.Right == nil {
				*counter = append(*counter, root.Val)
			}
			traversal(root.Right, counter)
		}
	}

	fL := make([]int, 0)
	fR := make([]int, 0)

	traversal(root1, &fL)
	traversal(root2, &fR)
	// if len(fL) != len(fR) {
	// 	return false
	// }
	// for i, j := 0, 0; i < len(fL) && j < len(fR); {
	// 	if fL[i] != fR[j] {
	// 		return false
	// 	}
	// 	i++
	// 	j++
	// }
	//
	// return true
	// or else
	return reflect.DeepEqual(fL, fR)
}
