// Your music player contains n different songs. You want to listen to goal songs (not necessarily different) during your trip. To avoid boredom, you will create a playlist so that:
//
// Every song is played at least once.
// A song can only be played again only if k other songs have been played.
// Given n, goal, and k, return the number of possible playlists that you can create. Since the answer can be very large, return it modulo 109 + 7.
package main

const (
	MOD = 1_000_000_007
)

var (
	dp [][]int
)

func numMusicPlaylists(n int, goal int, k int) int {
	// Your music player contains `n` different songs. You want to listen to `goal` songs
	// songs denotes `n` => 1,..,n
	// goal denotes the desired size of the playist
	dp = make([][]int, goal+1)
	for i := 0; i <= goal; i++ {
		dp[i] = make([]int, n+1)
		for j := 0; j <= n; j++ {
			dp[i][j] = -1
		}
	}
	return helperD(goal, n, k, n)
}

func helperD(i, j, k int, n int) int {
	if i == 0 && j == 0 {
		return 1
	}

	if i == 0 || j == 0 {
		return 0
	}

	if dp[i][j] != -1 {
		return dp[i][j]
	}

	var ans int
	ans = (helperD(i-1, j-1, k, n) * (n - j + 1)) % MOD
	if j > k {
		ans = (ans + helperD(i-1, j, k, n)*(j-k)) % MOD
	}
	dp[i][j] = ans
	return ans
}
