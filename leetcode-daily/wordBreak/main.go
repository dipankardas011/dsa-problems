// Given a string s and a dictionary of strings wordDict, return true if s can be segmented into a space-separated sequence of one or more dictionary words.
//
// Note that the same word in the dictionary may be reused multiple times in the segmentation.
package main

type set struct {
	data map[string]bool
}

func (m *set) create() {
	m.data = make(map[string]bool, 0)
}

func (m *set) add(key string) {
	m.data[key] = true
}

func (m *set) contains(key string) bool {
	_, ok := m.data[key]
	return ok
}

func wordBreak(s string, wordDict []string) bool {
	var hashSet *set = &set{}
	hashSet.create()
	for _, word := range wordDict {
		hashSet.add(word)
	}
	dp := make([]bool, len(s)+1)
	dp[0] = true
	for end := 1; end <= len(s); end++ {
		for start := 0; start < end; start++ {
			if dp[start] && hashSet.contains(s[start:end]) {
				dp[end] = true
				break
			}
		}
	}
	return dp[len(s)]
}
