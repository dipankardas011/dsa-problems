// Given an n x n array of integers matrix, return the minimum sum of any falling path through matrix.
//
// A falling path starts at any element in the first row and chooses the element in the next row that is either directly below or diagonally left/right. Specifically, the next element from position (row, col) will be (row + 1, col - 1), (row + 1, col), or (row + 1, col + 1).
package main

import "math"

func minFallingPathSum(matrix [][]int) int {
	m, n := len(matrix), len(matrix[0])
	for r := 1; r < m; r++ {
		for c := 0; c < n; c++ {
			switch c {
			case 0:
				matrix[r][c] += min(matrix[r-1][c], matrix[r-1][c+1]) // to the down or right
			case n - 1:
				matrix[r][c] += min(matrix[r-1][c-1], matrix[r-1][c]) // to the left or down
			default:
				matrix[r][c] += min(matrix[r-1][c-1], matrix[r-1][c], matrix[r-1][c+1]) // to the left, right, down
			}
		}
	}
	minRes := math.MaxInt
	for c := 0; c < n; c++ {
		minRes = min(minRes, matrix[m-1][c])
	}
	return minRes
}
