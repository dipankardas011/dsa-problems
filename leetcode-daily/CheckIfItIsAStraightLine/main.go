// You are given an array coordinates, coordinates[i] = [x, y], where [x, y] represents the coordinate of a point. Check if these points make a straight line in the XY plane.
package main

import "fmt"

// as m = (y2-y1)/(x2-x1) not used becaue of Inf value
// so ΔY1 * ΔX2 = ΔY2 * ΔX1
// ΔY1 / ΔX1 = ΔY2 / ΔX2
func checkStraightLine(coordinates [][]int) bool {
	if len(coordinates) == 2 {
		return true
	}
	x1 := coordinates[0][0]
	x2 := coordinates[1][0]

	y1 := coordinates[0][1]
	y2 := coordinates[1][1]

	deltaY := y2 - y1
	deltaX := x2 - x1

	for i, coordinate := range coordinates {
		if i >= 2 {
			x := coordinate[0]
			y := coordinate[1]
			deltaX_1 := x - x1
			deltaY_1 := y - y1

			if deltaX_1*deltaY != deltaY_1*deltaX {
				return false
			}

		}
	}
	return true
}

func main() {
	coordinates := [][]int{
		{0, 0},
		{0, 1},
		{0, -1},
	}
	fmt.Println(checkStraightLine(coordinates))
}
