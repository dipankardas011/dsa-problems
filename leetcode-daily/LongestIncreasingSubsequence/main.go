// Given an integer array nums, return the length of the longest strictly increasing
// subsequence
// .

package main

func lengthOfLIS(nums []int) int {
	memory := make([]int, len(nums))
	for i, _ := range nums {
		memory[i] = 1
	}

	ans := 1

	for i := 0; i < len(nums); i++ {
		for j := 0; j < i; j++ {
			if nums[j] < nums[i] {
				memory[i] = max(memory[i], memory[j]+1)
				ans = max(ans, memory[i])
			}
		}
	}
	// fmt.Println(nums)
	// fmt.Println(memory)
	return ans
}
