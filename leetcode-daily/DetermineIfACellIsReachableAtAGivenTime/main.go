// You are given four integers sx, sy, fx, fy, and a non-negative integer t.
//
// In an infinite 2D grid, you start at the cell (sx, sy). Each second, you must move to any of its adjacent cells.
//
// Return true if you can reach cell (fx, fy) after exactly t seconds, or false otherwise.
//
// A cell's adjacent cells are the 8 cells around it that share at least one corner with it. You can visit the same cell several times.
package main

import (
	"cmp"
	"fmt"
	"math"
)

func isReachableAtTime(sx int, sy int, fx int, fy int, t int) bool {
	w := int(math.Abs(float64(sx - fx)))
	h := int(math.Abs(float64(sy - fy)))

	if w == 0 && h == 0 && t == 1 {
		return false
	}

	return t >= max(w, h)
}

func max[T cmp.Ordered](a, b T) T {
	if a > b {
		return a
	}
	return b
}

func main() {
	fmt.Println(isReachableAtTime(2, 4, 7, 7, 6))
}
