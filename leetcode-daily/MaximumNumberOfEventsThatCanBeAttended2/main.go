// You are given an array of events where events[i] = [startDayi, endDayi, valuei]. The ith event starts at startDayi and ends at endDayi, and if you attend this event, you will receive a value of valuei. You are also given an integer k which represents the maximum number of events you can attend.
//
// You can only attend one event at a time. If you choose to attend an event, you must attend the entire event. Note that the end day is inclusive: that is, you cannot attend two events where one of them starts and the other ends on the same day.
//
// Return the maximum sum of values that you can receive by attending events.
package main

import (
	"sort"
)

// //////////// custom sort
type Event [][]int

func (s Event) Len() int {
	return len(s)
}

func (s Event) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s Event) Less(i, j int) bool {
	return s[i][0] < s[j][0]
}

//////////////

var dp [][]int

func maxValue(events [][]int, k int) int {
	sort.Sort(Event(events))

	size := len(events)
	dp = make([][]int, k+1)
	for i, _ := range dp {
		dp[i] = make([]int, size)
	}

	for i, _ := range dp {
		for j := 0; j < len(dp[i]); j++ {
			dp[i][j] = -1
		}
	}

	// calculate

	return helperMaxValue(events, 0, k, -1, 0)
}

func helperMaxValue(events [][]int, acceptItems, limitItems int, endingTime int, eventIdx int) int {

	if eventIdx == len(events) || acceptItems == limitItems {
		return 0
	}

	if events[eventIdx][0] <= endingTime {
		return helperMaxValue(events, acceptItems, limitItems, endingTime, eventIdx+1)
	}

	if dp[acceptItems][eventIdx] != -1 {
		return dp[acceptItems][eventIdx]
	}

	notTaken := helperMaxValue(events, acceptItems, limitItems, endingTime, eventIdx+1)

	taken := helperMaxValue(events, acceptItems+1, limitItems, events[eventIdx][1], eventIdx+1) + events[eventIdx][2]

	maximum := max(notTaken, taken)

	dp[acceptItems][eventIdx] = maximum

	return maximum

}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
