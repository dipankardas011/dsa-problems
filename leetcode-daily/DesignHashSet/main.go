package main

type MyHashSet struct {
	Set map[int]bool
}

func Constructor() MyHashSet {
	var ret MyHashSet = MyHashSet{}
	ret.Set = make(map[int]bool, 0)
	return ret
}

func (this *MyHashSet) Add(key int) {
	this.Set[key] = true
}

func (this *MyHashSet) Remove(key int) {
	this.Set[key] = false
	delete(this.Set, key)
}

func (this *MyHashSet) Contains(key int) bool {
	_, isPresent := this.Set[key]
	return isPresent
}

/**
 * Your MyHashSet object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Add(key);
 * obj.Remove(key);
 * param_3 := obj.Contains(key);
 */
