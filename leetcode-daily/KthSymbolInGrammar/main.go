// We build a table of n rows (1-indexed). We start by writing 0 in the 1st row. Now in every subsequent row, we look at the previous row and replace each occurrence of 0 with 01, and each occurrence of 1 with 10.
//
// For example, for n = 3, the 1st row is 0, the 2nd row is 01, and the 3rd row is 0110.
// Given two integer n and k, return the kth (1-indexed) symbol in the nth row of a table of n rows.
package main

import "fmt"

/*
 * 0 => 01
 * 1 => 10
 */

func treeTraversal(n, k int, root int) int {
	if n == 1 {
		return root
	}

	totalAvailableNodes := 1 << (n - 1)
	nextRoot := 0

	// if the k is in which side is it left child or in the right child
	// we also need to reduce the k to fix to the next subset by reducing by the half of the current nodes

	if k > (totalAvailableNodes / 2) {
		// means we need to reach out for the right child
		if root == 0 {
			nextRoot = 1
		} else {
			nextRoot = 0
		}
		return treeTraversal(n-1, k-(totalAvailableNodes/2), nextRoot)
	}

	// means we need to reac out for the left child
	if root == 1 {
		nextRoot = 1
	} else {
		nextRoot = 0
	}
	return treeTraversal(n-1, k, nextRoot)
}

func kthGrammar(n int, k int) int {
	return treeTraversal(n, k, 0)
}

func main() {
	fmt.Println(kthGrammar(1, 1))
}
