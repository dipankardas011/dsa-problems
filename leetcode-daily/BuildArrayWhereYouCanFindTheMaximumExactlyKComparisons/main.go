// You are given three integers n, m and k. Consider the following algorithm to find the maximum element of an array of positive integers:
//
//
// You should build the array arr which has the following properties:
//
// arr has exactly n integers.
// 1 <= arr[i] <= m where (0 <= i < n).
// After applying the mentioned algorithm to arr, the value search_cost is equal to k.
// Return the number of ways to build the array arr under the mentioned conditions. As the answer may grow large, the answer must be computed modulo 109 + 7.
//

package main

import "fmt"

const MOD int = 1e9 + 7

func numOfArrays(n int, m int, k int) int {
	mem := make([][][]int, n)
	for i := 0; i < n; i++ {
		mem[i] = make([][]int, m+1)
		for j := 0; j <= m; j++ {
			mem[i][j] = make([]int, k+1)
			for ij := 0; ij < k+1; ij++ {
				mem[i][j][ij] = -1
			}
		}
	}
	return calc(mem, n, m, 0, 0, k)
}

func calc(mem [][][]int, n, m, idx, currMax, leftK int) int {
	if idx == n {
		if leftK == 0 {
			return 1
		}
		return 0
	}

	if leftK < 0 {
		return 0
	}

	// check dp
	if mem[idx][currMax][leftK] != -1 {
		return mem[idx][currMax][leftK]
	}

	ans := 0
	// first try not to add new max
	for num := 1; num <= currMax; num++ {
		ans = (ans + calc(mem, n, m, idx+1, currMax, leftK)) % MOD
	}

	for num := currMax + 1; num <= m; num++ {
		ans = (ans + calc(mem, n, m, idx+1, num, leftK-1)) % MOD
	}

	// assign the dp
	mem[idx][currMax][leftK] = ans
	return ans
}

func main() {
	fmt.Println(numOfArrays(2, 3, 1))
}
