// You are given two non-empty linked lists representing two non-negative integers. The most significant digit comes first and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.
//
// You may assume the two numbers do not contain any leading zero, except the number 0 itself.
package main

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {

	n1 := make([]int, 0, 100)
	n2 := make([]int, 0, 100)

	h1 := l1
	for h1 != nil {
		n1 = append(n1, h1.Val)
		h1 = h1.Next
	}

	h2 := l2
	for h2 != nil {
		n2 = append(n2, h2.Val)
		h2 = h2.Next
	}

	digits := sum(n1, n2)

	var prev *ListNode = nil
	head := l1
	for i := len(digits) - 1; i >= 0; i-- {
		digit := digits[i]
		if head == nil {
			prev.Next = &ListNode{Val: digit, Next: nil}
			head = prev.Next
			prev = head
		}
		prev = head
		head.Val = digit
		head = head.Next
	}
	return l1
}

func sum(n1, n2 []int) []int {
	digits := make([]int, 0, 100)
	carry := 0
	i1, i2 := len(n1)-1, len(n2)-1
	for i1 >= 0 && i2 >= 0 {
		sum := n1[i1] + n2[i2] + carry
		digits = append(digits, sum%10)

		carry = sum / 10
		i1--
		i2--
	}
	for i1 >= 0 {
		sum := n1[i1] + carry
		digits = append(digits, sum%10)
		carry = sum / 10
		i1--
	}

	for i2 >= 0 {
		sum := n2[i2] + carry
		digits = append(digits, sum%10)
		carry = sum / 10
		i2--
	}
	if carry != 0 {
		digits = append(digits, carry)
	}
	return digits
}
