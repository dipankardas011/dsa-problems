// You are given a 0-indexed m x n binary matrix grid.
//
// A 0-indexed m x n difference matrix diff is created with the following procedure:
//
// Let the number of ones in the ith row be onesRowi.
// Let the number of ones in the jth column be onesColj.
// Let the number of zeros in the ith row be zerosRowi.
// Let the number of zeros in the jth column be zerosColj.
// diff[i][j] = onesRowi + onesColj - zerosRowi - zerosColj
// Return the difference matrix diff.
package main

func onesMinusZeros(grid [][]int) [][]int {
	noEleRow := len(grid[0])
	noEleCol := len(grid)

	rows := len(grid)
	cols := len(grid[0])

	countOnesRows := make([]int, rows)
	countOnesCols := make([]int, cols)

	diff := make([][]int, rows)
	for i := 0; i < rows; i++ {
		diff[i] = make([]int, cols)
	}

	for r := 0; r < rows; r++ {
		for c := 0; c < cols; c++ {
			if grid[r][c] == 1 {
				countOnesRows[r]++
			}
		}
	}
	for c := 0; c < cols; c++ {
		for r := 0; r < rows; r++ {
			if grid[r][c] == 1 {
				countOnesCols[c]++
			}
		}
	}

	for r := 0; r < rows; r++ {
		for c := 0; c < cols; c++ {
			diff[r][c] = countOnesRows[r] + countOnesCols[c] - (noEleRow - countOnesRows[r]) - (noEleCol - countOnesCols[c])
		}
	}
	return diff
}

func main() {}
