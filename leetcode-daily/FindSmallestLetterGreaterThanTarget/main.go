// You are given an array of characters letters that is sorted in non-decreasing order, and a character target. There are at least two different characters in letters.

// Return the smallest character in letters that is lexicographically greater than target. If such a character does not exist, return the first character in letters.
package main

func nextGreatestLetter(letters []byte, target byte) byte {
	left := 0
	right := len(letters) - 1
	savePoint := -1
	for left <= right {
		mid := left + (right-left)/2
		if letters[mid] > target {
			savePoint = mid
			right = mid - 1
		} else { // target >= letter[..]
			left = mid + 1
		}
	}
	if savePoint == -1 {
		return letters[0]
	}
	return letters[savePoint]
}
