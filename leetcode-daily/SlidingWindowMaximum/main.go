package main

func Max(a, b int) int {
	if a < b {
		return b
	}
	return a
}

func maxSlidingWindow(nums []int, k int) []int {
	max := nums[0]
	var arr []int = make([]int, (len(nums)-k)+1)

	for i := 0; i < k; i++ {
		max = Max(max, nums[i])
	}
	i := 0
	arr[i] = max
	i++
	// arr = append(arr, max)

	index := k
	removal := 1
	for index < len(nums) {

		if max == nums[removal-1] {
			max = nums[removal]
			for m := removal; m <= index; m++ {
				max = Max(max, nums[m])
			}
		}

		max = Max(max, nums[index])
		// arr = append(arr, max)
		arr[i] = max

		index++
		removal++
		i++
	}
	return arr
}
