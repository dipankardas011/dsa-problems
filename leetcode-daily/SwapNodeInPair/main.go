package main

//Given a linked list, swap every two adjacent nodes and return its head. You must solve the problem without modifying the values in the list's nodes (i.e., only nodes themselves may be changed.)

type ListNode struct {
	Val  int
	Next *ListNode
}

func swapPairs(head *ListNode) *ListNode {
	if head == nil {
		return nil
	}
	if head.Next == nil {
		return head
	}
	var temp *ListNode = head
	isFirst := true
	var prev *ListNode = nil
	for temp != nil && temp.Next != nil {
		var curr *ListNode = temp
		var next *ListNode = temp.Next
		curr.Next = next.Next
		next.Next = curr
		temp = curr.Next

		if prev != nil {
			prev.Next = next
		}
		if isFirst {
			head = next
			isFirst = false
		}
		prev = curr
	}
	return head
}
