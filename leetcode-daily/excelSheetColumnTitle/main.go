package main

import "strings"

func convertToTitle(columnNumber int) string {
	var str strings.Builder
	for columnNumber > 0 {
		columnNumber--
		str.WriteString(string(columnNumber%26 + 'A'))

		columnNumber /= 26
	}
	rev := str.String()
	return reverseString(rev)
}

func reverseString(s string) string {
	// Convert the string to a slice of runes
	runes := []rune(s)

	// Reverse the order of runes in the slice
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}

	// Convert the slice of runes back to a string
	reversedString := string(runes)
	return reversedString
}
