// Given an array of intervals intervals where intervals[i] = [starti, endi], return the minimum number of intervals you need to remove to make the rest of the intervals non-overlapping.
package main

import (
	"math"
	"sort"
)

type SortBy [][]int

func (a SortBy) Len() int           { return len(a) }
func (a SortBy) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a SortBy) Less(i, j int) bool { return a[i][1] < a[j][1] }

func eraseOverlapIntervals(intervals [][]int) int {
	sort.Sort(SortBy(intervals))

	ans := 0
	lastEnd := math.MinInt

	for _, interval := range intervals {
		if lastEnd <= interval[0] {
			lastEnd = interval[1]
		} else {
			ans++
		}
	}

	return ans
}
