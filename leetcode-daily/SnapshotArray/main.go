// Implement a SnapshotArray that supports the following interface:

// SnapshotArray(int length) initializes an array-like data structure with the given length. Initially, each element equals 0.
// void set(index, val) sets the element at the given index to be equal to val.
// int snap() takes a snapshot of the array and returns the snap_id: the total number of times we called snap() minus 1.
// int get(index, snap_id) returns the value at the given index, at the time we took the snapshot with the given snap_id
package main

type Snapshot struct {
	SnapID  int
	Element int
}

type SnapshotArray struct {
	CurrArr     []int
	SnapCount   int
	SnapShotArr [][]Snapshot
	IsChanged   []bool
}

func Constructor(length int) SnapshotArray {
	newSnapShot := SnapshotArray{}
	newSnapShot.CurrArr = make([]int, length)
	newSnapShot.IsChanged = make([]bool, length)
	newSnapShot.SnapCount = 0
	newSnapShot.SnapShotArr = make([][]Snapshot, length)
	return newSnapShot
}

func (this *SnapshotArray) Set(index int, val int) {
	this.IsChanged[index] = true
	this.CurrArr[index] = val
}

// the key step here to remember is that when that idex is going to change
// then only we will be adding during the Snap()
// so the snapID can look like this 1, 3, 4 so if the query is for 2
// it will be same as 1 also as its is sorted binary search can be used
func (this *SnapshotArray) Snap() int {
	this.SnapCount++
	for idx, change := range this.IsChanged {
		if change {
			// insert it into the existing list
			this.SnapShotArr[idx] = append(this.SnapShotArr[idx], Snapshot{SnapID: this.SnapCount - 1, Element: this.CurrArr[idx]})

			// make this unchanged again
			this.IsChanged[idx] = false

		}
	}
	return this.SnapCount - 1
}

func (this *SnapshotArray) Get(index int, snap_id int) int {
	getHistoryIdx := this.SnapShotArr[index]

	left := 0
	right := len(getHistoryIdx) - 1

	element := -1

	for left <= right {
		mid := left + (right-left)/2
		if getHistoryIdx[mid].SnapID == snap_id {
			return getHistoryIdx[mid].Element
		} else if getHistoryIdx[mid].SnapID < snap_id {
			// store result
			element = getHistoryIdx[mid].Element
			left = mid + 1
		} else {
			right = mid - 1
		}
	}

	return element
}

/**
 * Your SnapshotArray object will be instantiated and called as such:
 * obj := Constructor(length);
 * obj.Set(index,val);
 * param_2 := obj.Snap();
 * param_3 := obj.Get(index,snap_id);
 */
