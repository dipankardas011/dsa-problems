// There are a total of numCourses courses you have to take, labeled from 0 to numCourses - 1. You are given an array prerequisites where prerequisites[i] = [ai, bi] indicates that you must take course bi first if you want to take course ai.
//
// For example, the pair [0, 1], indicates that to take course 0 you have to first take course 1.
// Return true if you can finish all courses. Otherwise, return false.
package main

import "fmt"

type LL struct {
	Data int
	Next *LL
}

type Queue struct {
	head *LL
	tail *LL
	len  int
}

func (que *Queue) NewQueue() {
	que.head = nil
	que.tail = nil
	que.len = 0
}

func (que *Queue) PushQueue(data int) {
	if que.tail == nil {
		que.head = &LL{Data: data, Next: nil}
		que.tail = que.head
		que.len = 1
		return
	}

	que.tail.Next = &LL{Data: data, Next: nil}
	que.tail = que.tail.Next
	que.len += 1
}

func (que *Queue) PopQueue() (int, error) {
	if que.head == nil {
		return -1, fmt.Errorf("Empty Queue")
	}

	if que.head == que.tail {
		val := que.head.Data
		que.head, que.tail = nil, nil
		que.len = 0
		return val, nil
	}

	val := que.head.Data
	que.head = que.head.Next
	que.len -= 1
	return val, nil
}

func (que *Queue) LenQueue() int {
	return que.len
}

func canFinish(numCourses int, prerequisites [][]int) bool {
	que := Queue{}
	indegree := make([]int, numCourses)
	graph := make(map[int][]int, 0)
	for _, prerequisite := range prerequisites {
		graph[prerequisite[1]] = append(graph[prerequisite[1]], prerequisite[0])
		indegree[prerequisite[0]]++
	}

	for i := 0; i < numCourses; i++ {
		if indegree[i] == 0 {
			que.PushQueue(i)
		}
	}

	nodesVisited := 0

	for que.LenQueue() > 0 {
		vertex, _ := que.PopQueue()
		nodesVisited++

		for _, child := range graph[vertex] {
			indegree[child]--
			if indegree[child] == 0 {
				que.PushQueue(child)
			}
		}
	}

	if nodesVisited == numCourses {
		return true
	}
	return false

}
