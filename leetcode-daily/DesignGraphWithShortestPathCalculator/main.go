// There is a directed weighted graph that consists of n nodes numbered from 0 to n - 1. The edges of the graph are initially represented by the given array edges where edges[i] = [fromi, toi, edgeCosti] meaning that there is an edge from fromi to toi with the cost edgeCosti.
//
// Implement the Graph class:
//
// Graph(int n, int[][] edges) initializes the object with n nodes and the given edges.
// addEdge(int[] edge) adds an edge to the list of edges where edge = [from, to, edgeCost]. It is guaranteed that there is no edge between the two nodes before adding this one.
// int shortestPath(int node1, int node2) returns the minimum cost of a path from node1 to node2. If no path exists, return -1. The cost of a path is the sum of the costs of the edges in the path.

// If there is a significant imbalance between the frequency of shortestPath calls compared to the frequency of addEdge calls, the choice between using the Floyd-Warshall algorithm and Dijkstra's algorithm should be based on the number of times these two operations are performed:
//
// When shortestPath is called much more often than addEdge, it is more efficient to utilize the Floyd-Warshall algorithm.
// Conversely, if addEdge is called significantly more often than shortestPath, it is more practical to employ Dijkstra's algorithm for this problem.

package main

import (
	"container/heap"
	"fmt"
	"math"
)

// An Item is something we manage in a priority queue.
type Item struct {
	from  int
	cost  int // The priority of the item in the queue.
	index int // The index of the item in the heap.
}

type PriorityQueue []*Item

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	return pq[i].cost < pq[j].cost
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq *PriorityQueue) Push(x any) {
	n := len(*pq)
	item := x.(*Item)
	item.index = n
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() any {
	old := *pq
	n := len(old)
	item := old[n-1]
	old[n-1] = nil
	item.index = -1
	*pq = old[0 : n-1]
	return item
}

type Graph struct {
	graph [][][]int
	size  int
}

func Constructor(n int, edges [][]int) Graph {
	g := Graph{}
	g.graph = make([][][]int, n)
	g.size = n
	// initalize the graph
	for _, edge := range edges {
		g.graph[edge[0]] = append(g.graph[edge[0]], []int{edge[1], edge[2]})
	}
	return g
}

func (this *Graph) AddEdge(edge []int) {
	this.graph[edge[0]] = append(this.graph[edge[0]], []int{edge[1], edge[2]})
}

func (this *Graph) ShortestPath(node1 int, node2 int) int {
	size := this.size
	pq := make(PriorityQueue, 0)

	precedence := make([]int, size)
	distance := make([]int, size)
	for i := 0; i < size; i++ {
		precedence[i] = -1
		distance[i] = math.MaxInt32
	}

	pq = append(pq, &Item{from: node1, cost: 0})
	heap.Init(&pq)
	distance[node1] = 0

	for pq.Len() > 0 {
		extract := heap.Pop(&pq).(*Item)
		cost := extract.cost
		from := extract.from

		if cost > distance[from] {
			continue
		}
		if from == node2 {
			return cost
		}

		neighbourEdges := this.graph[from]

		for _, e := range neighbourEdges {
			to := e[0]
			w := e[1] + cost
			if w < distance[to] {
				distance[to] = w
				heap.Push(&pq, &Item{from: to, cost: w})
			}
		}
	}

	return -1
}

func main() {
	obj := Constructor(4, [][]int{
		{0, 2, 5}, {0, 1, 2}, {1, 2, 1}, {3, 0, 3},
	})
	fmt.Printf("%v", obj)
	obj.AddEdge([]int{1, 3, 4})

	fmt.Printf("%v", obj)
	fmt.Println(obj.ShortestPath(3, 2))
	fmt.Println(obj.ShortestPath(0, 3))
}
