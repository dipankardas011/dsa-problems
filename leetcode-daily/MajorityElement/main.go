// Given an array nums of size n, return the majority element.
//
// The majority element is the element that appears more than ⌊n / 2⌋ times. You may assume that the majority element always exists in the array.

package main

import "fmt"

func majorityElement(nums []int) int {
	counter := 1
	majorElement := nums[0]

	for i := 1; i < len(nums); i++ {
		if majorElement == nums[i] {
			counter++
		} else {
			counter--
		}
		if counter == 0 {
			majorElement = nums[i]
			counter = 1
		}
	}

	return majorElement
}

func main() {
	fmt.Println("vim-go")
}
