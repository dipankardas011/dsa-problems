// Given two strings s1 and s2, return the lowest ASCII sum of deleted characters to make two strings equal.
//
// Example 1:
//
// Input: s1 = "sea", s2 = "eat"
// Output: 231
// Explanation: Deleting "s" from "sea" adds the ASCII value of "s" (115) to the sum.
// Deleting "t" from "eat" adds 116 to the sum.
// At the end, both strings are equal, and 115 + 116 = 231 is the minimum sum possible to achieve this.
// Example 2:
//
// Input: s1 = "delete", s2 = "leet"
// Output: 403
// Explanation: Deleting "dee" from "delete" to turn the string into "let",
// adds 100[d] + 101[e] + 101[e] to the sum.
// Deleting "e" from "leet" adds 101[e] to the sum.
// At the end, both strings are equal to "let", and the answer is 100+101+101+101 = 403.
// If instead we turned both strings into "lee" or "eet", we would get answers of 433 or 417, which are higher.

package main

func minimumDeleteSum(s1 string, s2 string) int {
	sumS1 := 0
	sumS2 := 0
	for _, ch := range s1 {
		sumS1 += int(ch)
	}
	for _, ch := range s2 {
		sumS2 += int(ch)
	}

	lenR := len(s1)
	lenC := len(s2)

	dp := make([][]int, lenR+1)
	for i := 0; i <= lenR; i++ {
		dp[i] = make([]int, lenC+1)
	}

	for r := 1; r <= lenR; r++ {
		for c := 1; c <= lenC; c++ {
			if s1[r-1] == s2[c-1] {
				dp[r][c] = dp[r-1][c-1] + int(s1[r-1])
			} else {
				dp[r][c] = max(dp[r-1][c], dp[r][c-1])
			}
		}
	}

	return (sumS1 - dp[lenR][lenC]) + (sumS2 - dp[lenR][lenC])
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
