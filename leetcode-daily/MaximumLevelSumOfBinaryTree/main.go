// Given the root of a binary tree, the level of its root is 1, the level of its children is 2, and so on.

// Return the smallest level x such that the sum of all the values of nodes at level x is maximal.
package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

type Node struct {
	Node  *TreeNode
	Level int
	Next  *Node
}

type Queue struct {
	front *Node
	rear  *Node
}

func (this *Queue) Push(val *TreeNode, level int) {
	newNode := &Node{
		Node:  val,
		Level: level,
	}
	if this.front == nil {
		this.front = newNode
		this.rear = newNode
	} else {
		this.rear.Next = newNode
		newNode.Next = nil
		this.rear = newNode
	}
}

func (this *Queue) Pop() *Node {
	if this.front == nil {
		return nil
	}
	if this.front == this.rear {
		temp := this.front
		this.front = nil
		this.rear = nil
		return temp
	}
	temp := this.front
	this.front = this.front.Next
	return temp
}

func (this *Queue) IsEmpty() bool {
	if this.front == nil {
		return true
	}
	return false
}

func maxLevelSum(root *TreeNode) int {
	var que *Queue = &Queue{}
	que.Push(root, 1)
	sumLevel := make(map[int]int)

	for !que.IsEmpty() {

		node := que.Pop()
		if node == nil {
			return -1
		}
		level := node.Level
		sumLevel[level] += node.Node.Val

		if node.Node.Left != nil {
			que.Push(node.Node.Left, level+1)
		}
		if node.Node.Right != nil {
			que.Push(node.Node.Right, level+1)
		}
	}
	maxSum := -10000000
	resLevel := 1
	for level, sum := range sumLevel {
		if sum > maxSum {
			resLevel = level
			maxSum = sum
		}
	}
	return resLevel
}
