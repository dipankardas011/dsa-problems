// You are given an integer array pref of size n. Find and return the array arr of size n that satisfies:
//
// pref[i] = arr[0] ^ arr[1] ^ ... ^ arr[i].
// Note that ^ denotes the bitwise-xor operation.
//
// It can be proven that the answer is unique.
package main

import "fmt"

func findArray(pref []int) []int {
	ret := make([]int, len(pref))

	ret[0] = pref[0]
	for i := len(pref) - 1; i > 0; i-- {
		ret[i] = pref[i] ^ pref[i-1]
	}
	return ret
}

func main() {
	fmt.Println(findArray([]int{5, 2, 0, 3, 1}))
}
