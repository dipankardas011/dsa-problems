// Given a 0-indexed n x n integer matrix grid, return the number of pairs (ri, cj) such that row ri and column cj are equal.

// A row and column pair is considered equal if they contain the same elements in the same order (i.e., an equal array).
package main

import "fmt"

// using O(n^2)
func equalPairs(grid [][]int) int {
	hsMap := make(map[string]int)
	for r := 0; r < len(grid); r++ {
		rToStr := fmt.Sprintf("%v", grid[r])
		_, present := hsMap[rToStr]
		if present {
			hsMap[rToStr] += 1
		} else {
			hsMap[rToStr] = 1
		}
	}

	count := 0
	for c := 0; c < len(grid); c++ {
		col := make([]int, len(grid))
		for r := 0; r < len(grid); r++ {
			col[r] = grid[r][c]
		}

		cToStr := fmt.Sprintf("%v", col)
		val, present := hsMap[cToStr]
		if present {
			count += val
		}
	}
	return count
}

// using O(n^3)
func equalPairs11(grid [][]int) int {
	count := 0
	n := len(grid)

	for r := 0; r < n; r++ {
		for c := 0; c < n; c++ {
			match := true

			for i := 0; i < n; i++ {
				if grid[r][i] != grid[i][c] {
					match = false
					break
				}
			}

			if match {
				count += 1
			}
		}
	}
	return count
}
