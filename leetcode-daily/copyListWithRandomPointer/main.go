package main

import "fmt"

type node struct {
	Val    int
	Next   *node
	Random *node
}

func (n *node) String() string {
	return fmt.Sprintf("{ %p }| val: %d | Next: %p | Random: %p |\n", n, n.Val, n.Next, n.Random)
}

func copyRandomList(head *node) *node {
	if head == nil {
		return nil
	}
	var newHead *node = nil

	hsMap := make(map[*node]*node, 0)

	iter := head

	// for first element
	newHead = &node{
		Val:    iter.Val,
		Next:   nil,
		Random: iter.Random,
	}
	hsMap[iter] = newHead

	iter = iter.Next
	///

	for ; iter != nil; iter = iter.Next {
		tt := newHead
		for tt.Next != nil {
			tt = tt.Next
		}
		newNode := &node{
			Val:    iter.Val,
			Next:   nil,
			Random: iter.Random,
		}
		tt.Next = newNode
		hsMap[iter] = newNode
	}

	for iter := newHead; iter != nil; iter = iter.Next {
		if iter.Random != nil {
			random, _ := hsMap[iter.Random]
			iter.Random = random
		}
	}

	for i := head; i != nil; i = i.Next {
		fmt.Println(i)
	}

	for i := newHead; i != nil; i = i.Next {
		fmt.Println(i)
	}

	return newHead
}
