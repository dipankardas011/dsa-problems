// Given the root of a binary tree, find the maximum value v for which there exist different nodes a and b where v = |a.val - b.val| and a is an ancestor of b.
//
// A node a is an ancestor of b if either: any child of a is equal to b or any child of a is an ancestor of b.
package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func maxAncestorDiff(root *TreeNode) int {
	var calc func(*TreeNode, int, int) int
	calc = func(root *TreeNode, minV int, maxV int) int {
		if root == nil {
			return maxV - minV
		}
		minV = min(minV, root.Val)
		maxV = max(maxV, root.Val)
		rL := calc(root.Left, minV, maxV)
		rR := calc(root.Right, minV, maxV)
		return max(rL, rR)
	}
	if root == nil {
		return 0
	}
	return calc(root, root.Val, root.Val)
}
