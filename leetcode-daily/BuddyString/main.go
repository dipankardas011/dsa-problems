package main

import (
	"strings"
)

func buddyStrings(s string, goal string) bool {
	if len(s) != len(goal) {
		return false
	}

	if strings.Compare(s, goal) == 0 {

		var freq []int = make([]int, 26)
		for _, char := range s {
			freq[int(char)-'a'] += 1
			if freq[int(char)-'a'] == 2 {
				return true
			}
		}
		return false
	}

	first := -1
	second := -1

	for i := 0; i < len(s); i++ {
		if s[i] != goal[i] {
			if first == -1 {
				first = i
			} else if second == -1 {
				second = i
			} else {
				return false
			}
		}
	}

	if second == -1 {
		return false
	}

	return s[first] == goal[second] &&
		s[second] == goal[first]
}
