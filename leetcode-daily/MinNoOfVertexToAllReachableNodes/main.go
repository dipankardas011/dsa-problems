// Given a directed acyclic graph, with n vertices numbered from 0 to n-1, and an array edges where edges[i] = [fromi, toi] represents a directed edge from node fromi to node toi.

// Find the smallest set of vertices from which all nodes in the graph are reachable. It's guaranteed that a unique solution exists.

// Notice that you can return the vertices in any order.
func findSmallestSetOfVertices(n int, edges [][]int) []int {

	count := make(map[int]int, 0)

	for i := 0; i < len(edges); i++ {
		to := edges[i][1]
		count[to]++
	}

	size := n - len(count)
	// for V := 0; V < n; V++ {
	// 	if count[V] == 0 {
	// 		size++
	// 	}
	// }

	ret := make([]int, size)
	i := 0

	for V := 0; V < n; V++ {
		if count[V] == 0 {
			ret[i] = V
			i++
		}
	}
	return ret
}
