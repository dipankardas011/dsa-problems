// Given an array of integers arr, return true if the number of occurrences of each value in the array is unique or false otherwise.
package main

import "fmt"

func uniqueOccurrences(arr []int) bool {
	count := make(map[int]int)
	for _, ar := range arr {
		count[ar]++
	}

	sameOrNot := make(map[int]bool)
	for _, v := range count {
		if sameOrNot[v] {
			return false
		} else {
			sameOrNot[v] = true
		}
	}
	return true
}
func main() {
	fmt.Println("vim-go")
}
