// You are given a string num representing a large integer. An integer is good if it meets the following conditions:
//
// It is a substring of num with length 3.
// It consists of only one unique digit.
// Return the maximum good integer as a string or an empty string "" if no such integer exists.
//
// Note:
//
// A substring is a contiguous sequence of characters within a string.
// There may be leading zeroes in num or a good integer.
package main

import (
	"strconv"
	"strings"
)

func largestGoodInteger(num string) string {
	maxDigit := -1
	for i := 0; i < len(num)-2; i++ {
		if num[i] == num[i+1] && num[i] == num[i+2] {
			dEqu := int(rune(num[i]) - '0')
			if dEqu > maxDigit {
				maxDigit = dEqu
			}
		}
	}
	if maxDigit == -1 {
		return ""
	}
	var str strings.Builder
	for i := 0; i < 3; i++ {
		str.WriteString(strconv.Itoa(maxDigit))
	}
	return str.String()
}
