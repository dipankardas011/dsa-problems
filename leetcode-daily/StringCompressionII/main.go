package main

import (
	"fmt"
	"strconv"
)

type Pair struct {
	start, k int
}

func getLengthOfOptimalCompression(s string, k int) int {
	n := len(s)
	var mem map[Pair]int = make(map[Pair]int)

	var dp func(int, int) int

	dp = func(start, k int) int {
		if start == n || n-start <= k {
			return 0
		}

		if v, ok := mem[Pair{start: start, k: k}]; ok {
			return v
		}

		count := [26]int{}
		mostFreq := 0
		ans := 10001
		for j := start; j < n; j++ {
			ch := rune(s[j])
			count[ch-'a']++
			mostFreq = max(mostFreq, count[ch-'a'])

			compressedLength := 1

			if mostFreq > 1 {
				compressedLength = 1 + len(strconv.Itoa(mostFreq))
			}

			if k >= (j - start + 1 - mostFreq) {
				ans = min(ans, compressedLength+dp(j+1, k-(j-start+1-mostFreq)))
			}

		}
		mem[Pair{start: start, k: k}] = ans
		return ans
	}

	return dp(0, k)
}

func main() {
	fmt.Println(getLengthOfOptimalCompression("abbbbbbbbbba", 2))
}
