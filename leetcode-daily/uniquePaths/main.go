// There is a robot on an m x n grid. The robot is initially located at the top-left corner (i.e., grid[0][0]). The robot tries to move to the bottom-right corner (i.e., grid[m - 1][n - 1]). The robot can only move either down or right at any point in time.
//
// Given the two integers m and n, return the number of possible unique paths that the robot can take to reach the bottom-right corner.
//
// The test cases are generated so that the answer will be less than or equal to 2 * 109.

package main

func uniquePaths(m int, n int) int {
	// mem := make([][]int, m)
	// for i:=0;i<m;i++ {
	//     mem[i] = make([]int, n)
	//     for j :=0;j<n;j++ {
	//         mem[i][j] = -1
	//     }
	// }
	// _ = execMem(0,0,m,n, mem)
	// return mem[0][0]

	// for tabulation
	return execTabulation(m, n)
}

func execTabulation(m, n int) int {
	mem := make([][]int, m)
	for i := 0; i < m; i++ {
		mem[i] = make([]int, n)
	}
	for i := 0; i < m; i++ {
		mem[i][0] = 1
	}
	for j := 0; j < n; j++ {
		mem[0][j] = 1
	}
	for row := 1; row < m; row++ {
		for col := 1; col < n; col++ {
			mem[row][col] = mem[row-1][col] + mem[row][col-1]
		}
	}
	return mem[m-1][n-1]
}

func execMem(i, j int, m, n int, mem [][]int) int {
	if i < m && j < n {
		if i == m-1 && j == n-1 {
			mem[i][j] = 1
			return 1
		}
		if mem[i][j] != -1 {
			return mem[i][j]
		}
		down := execMem(i+1, j, m, n, mem)
		right := execMem(i, j+1, m, n, mem)
		mem[i][j] = right + down
		return right + down
	} else {
		return 0
	}
}
