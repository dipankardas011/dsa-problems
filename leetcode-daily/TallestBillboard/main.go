// You are installing a billboard and want it to have the largest height. The billboard will have two steel supports, one on each side. Each steel support must be an equal height.

// You are given a collection of rods that can be welded together. For example, if you have rods of lengths 1, 2, and 3, you can weld them together to make a support of length 6.

// Return the largest possible height of your billboard installation. If you cannot support the billboard, return 0.
package main

import "fmt"

func tallestBillboard(rods []int) int {
	mapCache := make(map[string]int)
	return helper(rods, 0, 0, mapCache)
}

func helper(rods []int, index, diff int, cache map[string]int) int {
	if index == len(rods) {
		if diff == 0 {
			return 0
		} else {
			return -2147483648
		}
	}

	key := fmt.Sprintf("%d+%d", index, diff)

	if val, ok := cache[key]; ok {
		return val
	}

	exclude := helper(rods, index+1, diff, cache)
	taller := helper(rods, index+1, diff+rods[index], cache) + rods[index]
	shorter := helper(rods, index+1, diff-rods[index], cache)

	maxHeight := max(exclude, max(taller, shorter))

	cache[key] = maxHeight
	return maxHeight
}

func max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}
