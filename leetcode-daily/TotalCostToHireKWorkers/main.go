package main

import (
	"container/heap"
)

type Item struct {
	value    int
	priority int
	index    int
}

type PriorityQueue []*Item

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	return pq[i].priority < pq[j].priority
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq *PriorityQueue) Push(x any) {
	n := len(*pq)
	item := x.(*Item)
	item.index = n
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Top() any {
	old := *pq
	item := old[0]
	item.index = -1
	return item
}

func (pq *PriorityQueue) Pop() any {
	old := *pq
	n := len(old)
	item := old[n-1]
	old[n-1] = nil  // avoid memory leak
	item.index = -1 // for safety
	*pq = old[0 : n-1]
	return item
}

func max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

func BuildPQ(costs []int, no int, reverse bool) *PriorityQueue {
	if reverse {
		no = max(no, len(costs)-no)
	}
	pq := make(PriorityQueue, 0)

	i := 0
	if !reverse {
		for idx := 0; idx < no; idx++ {
			pq = append(pq, &Item{
				value:    costs[idx],
				priority: costs[idx],
				index:    i,
			})
			i++
		}
	} else {
		for idx := no; idx < len(costs); idx++ {
			pq = append(pq, &Item{
				value:    costs[idx],
				priority: costs[idx],
				index:    i,
			})
			i++
		}
	}

	heap.Init(&pq)

	return &pq
}

func GetMin(pq1, pq2 *PriorityQueue, leftNext, rightNext *int, costs []int) int {
	var top1, top2 *Item
	if pq1.Len() > 0 {
		top1 = pq1.Top().(*Item)
	} else {
		top1 = nil
	}

	if pq2.Len() > 0 {
		top2 = pq2.Top().(*Item)
	} else {
		top2 = nil
	}

	ret := 0

	if top1 != nil && top2 == nil {

		_ = heap.Pop(pq1).(*Item)
		if *leftNext <= *rightNext {
			heap.Push(pq1, &Item{value: costs[*leftNext], priority: costs[*leftNext]})
			*leftNext = *leftNext + 1
		}
		ret = top1.value

	} else if top1 == nil && top2 != nil {
		_ = heap.Pop(pq2).(*Item)
		if *leftNext <= *rightNext {
			heap.Push(pq2, &Item{value: costs[*rightNext], priority: costs[*rightNext]})
			*rightNext = *rightNext - 1
		}
		ret = top2.value
	} else {

		if top1.value <= top2.value {

			_ = heap.Pop(pq1).(*Item)
			if *leftNext <= *rightNext {
				heap.Push(pq1, &Item{value: costs[*leftNext], priority: costs[*leftNext]})
				*leftNext = *leftNext + 1
			}
			ret = top1.value
		} else {

			_ = heap.Pop(pq2).(*Item)
			if *leftNext <= *rightNext {
				heap.Push(pq2, &Item{value: costs[*rightNext], priority: costs[*rightNext]})
				*rightNext = *rightNext - 1
			}
			ret = top2.value
		}
	}

	return ret
}

func totalCost(costs []int, k int, candidates int) int64 {
	left := BuildPQ(costs, candidates, false)
	right := BuildPQ(costs, candidates, true)
	leftN := candidates
	rightN := len(costs) - candidates - 1
	sum := int64(0)

	for k > 0 {
		k--
		sum += int64(GetMin(left, right, &leftN, &rightN, costs))
	}

	return sum
}

// func main() {
// 	fmt.Println("Cost: ", totalCost([]int{18, 64, 12, 21, 21, 78, 36, 58, 88, 58, 99, 26, 92, 91, 53, 10, 24, 25, 20, 92, 73, 63, 51, 65, 87, 6, 17, 32, 14, 42, 46, 65, 43, 9, 75}, 13, 23))
// }
