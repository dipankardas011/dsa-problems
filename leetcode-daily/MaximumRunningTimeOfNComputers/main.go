// You have n computers. You are given the integer n and a 0-indexed integer array batteries where the ith battery can run a computer for batteries[i] minutes. You are interested in running all n computers simultaneously using the given batteries.
//
// Initially, you can insert at most one battery into each computer. After that and at any integer time moment, you can remove a battery from a computer and insert another battery any number of times. The inserted battery can be a totally new battery or a battery from another computer. You may assume that the removing and inserting processes take no time.
//
// Note that the batteries cannot be recharged.
//
// Return the maximum number of minutes you can run all the n computers simultaneously.
package main

import (
	"sort"
)

func maxRunTime(n int, batteries []int) int64 {
	sort.Ints(batteries)

	var extra int64 = 0
	for i := 0; i < len(batteries)-n; i++ {
		extra += int64(batteries[i])
	}

	live := make([]int64, n)

	for i := len(batteries) - n; i < len(batteries); i++ {
		live[i-(len(batteries)-n)] = int64(batteries[i])
	}

	for i := 0; i < n-1; i++ {
		if extra < int64(i+1)*(live[i+1]-live[i]) {
			return live[i] + (extra / int64(i+1)) // it divides the total by number of computers needed to add the minutes
		}

		extra -= int64(i+1) * (live[i+1] - live[i])
	}

	if extra != 0 {
		return live[n-1] + extra/int64(n)
	}

	return live[n-1]
}
