package main

func minimumReplacement(nums []int) int64 {
	var ret int64 = 0

	for i := len(nums) - 2; i >= 0; i-- {
		if nums[i] <= nums[i+1] {
			continue
		}

		// NOTE: magic calculations
		// num_elements = (nums[i] + nums[i + 1] - 1) // nums[i + 1] is basically ceiling division. Take a and b for example. (a + b - 1) is the ceiling division of a divided by b.
		var count int64 = int64(nums[i]+nums[i+1]-1) / int64(nums[i+1])
		ret += count - 1

		nums[i] = nums[i] / int(count)
		/////////
	}

	return ret
}
