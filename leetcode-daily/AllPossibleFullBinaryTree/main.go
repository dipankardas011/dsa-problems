// Given an integer n, return a list of all possible full binary trees with n nodes. Each node of each tree in the answer must have Node.val == 0.
//
// Each element of the answer is the root node of one possible tree. You may return the final list of trees in any order.
//
// A full binary tree is a binary tree where each node has exactly 0 or 2 children.
package main

func allPossibleFBT(n int) []*TreeNode {
	memory := make(map[int][]*TreeNode, 0)
	return memFBT(n, memory)
}

func memFBT(n int, memory map[int][]*TreeNode) []*TreeNode {
	if n == 1 {
		leaf := make([]*TreeNode, 1)
		leaf[0] = &TreeNode{}
		return leaf
	}

	if val, present := memory[n]; present {
		return val
	}

	res := make([]*TreeNode, 0)

	for noChildLeft := 1; noChildLeft < n; noChildLeft += 2 {
		noChildRight := n - noChildLeft - 1 // 1 for counting the root

		left := memFBT(noChildLeft, memory)
		right := memFBT(noChildRight, memory)

		for _, l := range left {
			for _, r := range right {
				res = append(res, &TreeNode{Val: 0, Left: l, Right: r})
			}
		}
	}

	memory[n] = res

	return res
}
