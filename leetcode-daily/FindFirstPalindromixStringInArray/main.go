// Given an array of strings words, return the first palindromic string in the array. If there is no such string, return an empty string "".
//
// A string is palindromic if it reads the same forward and backward.

package main

func firstPalindrome(words []string) string {
	for _, word := range words {
		if checkIfPalindrome(word) {
			return word
		}
	}
	return ""
}

func checkIfPalindrome(str string) bool {
	r := len(str) - 1
	l := 0
	for l < r {
		if str[l] != str[r] {
			return false
		}
		l++
		r--
	}
	return true
}
