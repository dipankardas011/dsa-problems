package main

import "fmt"

func getPalindrome(s string, res *string, resLen *int, l, r int) {
	for l >= 0 && r < len(s) && s[l] == s[r] {
		if (r - l + 1) > *resLen {
			*res = s[l : r+1]
			*resLen = (r - l + 1)
		}
		l--
		r++
	}
}

func longestPalindrome(s string) string {
	res := ""
	resLen := 0

	for i := range s {
		// odd length
		getPalindrome(s, &res, &resLen, i, i)
		// even length
		getPalindrome(s, &res, &resLen, i, i+1)
	}

	return res
}

func main() {
	fmt.Println(longestPalindrome("babad"))
}
