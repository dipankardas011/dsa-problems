// Given an integer array nums, return the number of longest increasing subsequences.
//
// Notice that the sequence has to be strictly increasing.
//

package main

func findNumberOfLIS(nums []int) int {
	n := len(nums)

	length := make([]int, n)
	count := make([]int, n)

	for i, _ := range length {
		length[i] = 1
	}
	for i, _ := range count {
		count[i] = 1
	}

	for i := 0; i < n; i++ {
		for j := 0; j < i; j++ {
			if nums[j] < nums[i] {
				if length[i] < length[j]+1 {
					count[i] = 0
					length[i] = length[j] + 1
				}
				if length[i] == length[j]+1 {
					count[i] += count[j]
				}
			}
		}
	}
	m := 0
	counter := 0
	for _, l := range length {
		m = max(m, l)
	}

	for i := 0; i < n; i++ {
		if m == length[i] {
			counter += count[i]
		}
	}
	return counter
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
