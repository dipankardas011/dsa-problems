// Design a system that manages the reservation state of n seats that are numbered from 1 to n.
//
// Implement the SeatManager class:
//
// SeatManager(int n) Initializes a SeatManager object that will manage n seats numbered from 1 to n. All seats are initially available.
// int reserve() Fetches the smallest-numbered unreserved seat, reserves it, and returns its number.
// void unreserve(int seatNumber) Unreserves the seat with the given seatNumber
package main

import (
	"container/heap"
	"fmt"
)

// An Item is something we manage in a priority queue.
type Item struct {
	priority int // The priority of the item in the queue.
	// The index is needed by update and is maintained by the heap.Interface methods.
	index int // The index of the item in the heap.
}

type PriorityQueue []*Item

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	return pq[i].priority < pq[j].priority
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq *PriorityQueue) Push(x any) {
	n := len(*pq)
	item := x.(*Item)
	item.index = n
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() any {
	old := *pq
	n := len(old)
	item := old[n-1]
	old[n-1] = nil  // avoid memory leak
	item.index = -1 // for safety
	*pq = old[0 : n-1]
	return item
}

type SeatManager struct {
	pq PriorityQueue
}

func Constructor(n int) SeatManager {
	obj := SeatManager{}

	obj.pq = make(PriorityQueue, n)

	for i := 0; i < n; i++ {
		obj.pq[i] = &Item{
			priority: i + 1,
			index:    i,
		}
	}

	heap.Init(&obj.pq)
	return obj
}

func (this *SeatManager) Reserve() int {
	pollX := heap.Pop(&this.pq).(*Item)

	return pollX.priority
}

func (this *SeatManager) Unreserve(seatNumber int) {
	heap.Push(&this.pq, &Item{priority: seatNumber})
}

/**
 * Your SeatManager object will be instantiated and called as such:
 * obj := Constructor(n);
 * param_1 := obj.Reserve();
 * obj.Unreserve(seatNumber);
 */

func main() {

	obj := Constructor(4)
	fmt.Println(obj.Reserve())
	obj.Unreserve(1)
	fmt.Println("null")
	fmt.Println(obj.Reserve())
	fmt.Println(obj.Reserve())
	fmt.Println(obj.Reserve())
	obj.Unreserve(2)
	fmt.Println("null")

	fmt.Println(obj.Reserve())
	obj.Unreserve(1)
	fmt.Println("null")

	fmt.Println(obj.Reserve())
	obj.Unreserve(2)
	fmt.Println("null")
	// obj := Constructor(5)
	// fmt.Println("null")
	// fmt.Println(obj.Reserve())
	// fmt.Println(obj.Reserve())
	// obj.Unreserve(2)
	// fmt.Println("null")
	//
	// fmt.Println(obj.Reserve())
	// fmt.Println(obj.Reserve())
	// fmt.Println(obj.Reserve())
	// fmt.Println(obj.Reserve())
	//
	// obj.Unreserve(5)
	// fmt.Println("null")
}
