package main

// You are given an array of variable pairs equations and an array of real numbers values, where equations[i] = [Ai, Bi] and values[i] represent the equation Ai / Bi = values[i]. Each Ai or Bi is a string that represents a single variable.
// You are also given some queries, where queries[j] = [Cj, Dj] represents the jth query where you must find the answer for Cj / Dj = ?.
// Return the answers to all queries. If a single answer cannot be determined, return -1.0.
// Note: The input is always valid. You may assume that evaluating the queries will not result in division by zero and that there is no contradiction.
var (
	gCost float64
)

func calcEquation(equations [][]string, values []float64, queries [][]string) []float64 {
	vertexStore := make(map[string]bool, 0)
	for _, pair := range equations {
		from := pair[0]
		to := pair[1]
		vertexStore[from] = true
		vertexStore[to] = true
	}
	vertexIdx := make(map[string]int, 0)
	index := 0
	for k, _ := range vertexStore {
		vertexIdx[k] = index
		index++
	}
	// V -> graph vertices
	V := len(vertexIdx)
	graph := make([][]float64, V)
	for i := 0; i < V; i++ {
		graph[i] = make([]float64, V)
	}

	for pairIdx, pair := range equations {
		from := vertexIdx[pair[0]]
		to := vertexIdx[pair[1]]
		graph[from][to] = values[pairIdx]
		graph[to][from] = 1.0 / values[pairIdx]
	}

	ret := make([]float64, len(queries))
	for queryIdx, query := range queries {
		fromS := query[0]
		toS := query[1]
		if !vertexStore[fromS] || !vertexStore[toS] {
			ret[queryIdx] = -1.0
		} else if fromS == toS {
			ret[queryIdx] = 1.0
		} else {
			isVisited := make([]bool, V)
			cost := 1.0
			gCost = 1.0
			found := dfs(graph, vertexIdx[fromS], vertexIdx[toS], isVisited, cost)
			if found {
				ret[queryIdx] = gCost
			} else {
				ret[queryIdx] = -1.0
			}
		}
	}

	return ret
}

func dfs(graph [][]float64, vertexFrom, vertexTo int, isVisited []bool, cost float64) bool {
	isVisited[vertexFrom] = true
	V := len(graph)

	for i := 0; i < V; i++ {
		if !isVisited[i] && graph[vertexFrom][i] != 0 {
			cost = cost * graph[vertexFrom][i]
			if vertexTo == i {
				gCost = cost
				return true
			}
			a := dfs(graph, i, vertexTo, isVisited, cost)
			if a {
				return true
			}
			cost = cost / graph[vertexFrom][i]
		}
	}
	return false
}
