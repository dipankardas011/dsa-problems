// You are given an array prices where prices[i] is the price of a given stock on the ith day, and an integer fee representing a transaction fee.

// Find the maximum profit you can achieve. You may complete as many transactions as you like, but you need to pay the transaction fee for each transaction.

// Note: You may not engage in multiple transactions simultaneously (i.e., you must sell the stock before you buy again).

package main

func max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

func maxProfit(price []int, fee int) int {
	n := len(price)
	free := make([]int, n)
	hold := make([]int, n)

	// first we need to buy the [0] stock
	hold[0] = -price[0]

	for i := 1; i < n; i++ {
		hold[i] = max(hold[i-1], free[i-1]-price[i])
		free[i] = max(free[i-1], hold[i-1]+price[i]-fee)
	}

	return free[n-1]
}
