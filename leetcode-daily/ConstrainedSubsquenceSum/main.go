package main

import (
	"fmt"

	pq "github.com/emirpasic/gods/queues/priorityqueue"
	"github.com/emirpasic/gods/utils"
)

type Element []int

func byPriority(a, b interface{}) int {
	priorityA := a.(Element)[0]
	priorityB := b.(Element)[0]
	return -utils.IntComparator(priorityA, priorityB) // "-" descending order
}

func constrainedSubsetSum(nums []int, k int) int {
	queue := pq.NewWith(byPriority) // empty
	queue.Enqueue(Element([]int{nums[0], 0}))

	ans := nums[0]

	for i := 1; i < len(nums); i++ {

		for {
			if p, ok := queue.Peek(); ok {
				if i-p.(Element)[1] > k {
					queue.Dequeue()
				} else {
					break
				}
			} else {
				break
			}
		}

		if p, ok := queue.Peek(); ok {
			curr := max(0, p.(Element)[0]) + nums[i]
			ans = max(ans, curr)
			queue.Enqueue(Element([]int{curr, i}))
		}

	}

	return ans
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
func main() {
	fmt.Println(constrainedSubsetSum([]int{10, 2, -10, 5, 20}, 2))
}
