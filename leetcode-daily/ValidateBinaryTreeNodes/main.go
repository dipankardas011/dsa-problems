// You have n binary tree nodes numbered from 0 to n - 1 where node i has two children leftChild[i] and rightChild[i], return true if and only if all the given nodes form exactly one valid binary tree.
//
// If node i has no left child then leftChild[i] will equal -1, similarly for the right child.
//
// Note that the nodes have no values and that we only use the node numbers in this problem.
package main

import "fmt"

func validateBinaryTreeNodes(n int, leftChild []int, rightChild []int) bool {
	parent := make([]int, n)
	for i := range parent {
		parent[i] = -1
	}

	for p, c := range leftChild {
		if c == -1 {
			continue
		}
		if parent[c] != -1 {
			return false
		}
		parent[c] = p
	}

	for p, c := range rightChild {
		if c == -1 {
			continue
		}
		if parent[c] != -1 {
			return false
		}
		parent[c] = p
	}

	count := 0
	root := -1
	for p, val := range parent {
		if val == -1 {
			count++
			if count == 1 {
				root = p
			}
		}
	}
	if count != 1 {
		return false
	}

	var countNodes func([]int, []int, int) int

	countNodes = func(left, right []int, node int) int {
		if node == -1 {
			return 0
		}
		return 1 + countNodes(left, right, left[node]) + countNodes(left, right, right[node])
	}

	return countNodes(leftChild, rightChild, root) == n
}

func main() {
	fmt.Println(validateBinaryTreeNodes(4, []int{1, -1, 3, -1}, []int{2, -1, -1, -1}))
}
