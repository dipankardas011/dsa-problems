// Given an integer array of size n, find all elements that appear more than ⌊ n/3 ⌋ times.
//

package main

import "fmt"

func majorityElement(nums []int) []int {
	// as the count of number of ele > n/3
	// means if at most 2 elements as its  > n/3
	// would it be == n/3 it would be 3 elements at the max

	res := []int{}

	cnt1, cnt2, num1, num2 := 0, 0, 0, 0
	for _, num := range nums {
		if num == num1 {
			cnt1++
		} else if num == num2 {
			cnt2++
		} else if cnt1 == 0 {
			num1 = num
			cnt1 = 1
		} else if cnt2 == 0 {
			num2 = num
			cnt2 = 1
		} else {
			cnt1--
			cnt2--
		}
	}

	cnt1 = 0
	cnt2 = 0
	for _, num := range nums {
		if num1 == num {
			cnt1++
		} else if num2 == num {
			cnt2++
		}
	}

	if cnt1 > len(nums)/3 {
		res = append(res, num1)
	}
	if cnt2 > len(nums)/3 {
		res = append(res, num2)
	}

	return res
}

func main() {
	fmt.Println(majorityElement([]int{}))
}
