package main

import (
	"math"
	"sort"
)

type SortBy [][]int

func (a SortBy) Len() int           { return len(a) }
func (a SortBy) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a SortBy) Less(i, j int) bool { return a[i][1] < a[j][1] }

func findLongestChain(pairs [][]int) int {

	sort.Sort(SortBy(pairs))

	ans := 0
	last := math.MinInt
	for _, pair := range pairs {
		if last < pair[0] {
			last = pair[1]
			ans++
		}
	}
	return ans
}
