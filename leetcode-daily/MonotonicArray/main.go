// An array is monotonic if it is either monotone increasing or monotone decreasing.
//
// An array nums is monotone increasing if for all i <= j, nums[i] <= nums[j]. An array nums is monotone decreasing if for all i <= j, nums[i] >= nums[j].
//
// Given an integer array nums, return true if the given array is monotonic, or false otherwise.
package main

import "fmt"

func isMonotonic(nums []int) bool {
	if len(nums) == 1 {
		return true
	}
	return increasing(nums) || decreasing(nums)
}

func increasing(nums []int) bool {
	for i := 1; i < len(nums); i++ {
		j := i - 1
		if nums[j] > nums[i] {
			return false
		}
	}
	return true
}

func decreasing(nums []int) bool {
	for i := 1; i < len(nums); i++ {
		j := i - 1
		if nums[j] < nums[i] {
			return false
		}
	}
	return true
}

func main() {
	fmt.Println(isMonotonic([]int{1, 2, 2, 3}))
	fmt.Println(isMonotonic([]int{6, 5, 4, 4}))
}
