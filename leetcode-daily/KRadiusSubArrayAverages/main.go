package main

import "fmt"

func isSafe(i, k, size int) bool {
	return i-k >= 0 && i+k < size
}

func generatePrefix(nums []int) []int64 {
	prefix := make([]int64, len(nums)+1)
	prefix[0] = int64(nums[0])
	for i := 0; i < len(nums); i++ {
		prefix[i+1] = prefix[i] + int64(nums[i])
	}

	return prefix
}

func getAveragePrefix(prefix []int64, i, k int) int {
	sum := prefix[i+k+1] - prefix[i-k]
	return int(sum / int64(2*k+1))
}

func getAverage(nums []int, i, k int) int {
	var sum int64 = 0
	left := i - k
	right := i + k
	for left < right {
		sum += int64(nums[left]) + int64(nums[right])
		left++
		right--
	}
	sum += int64(nums[left])
	return int(sum / int64(2*k+1))
}

func getAverages(nums []int, k int) []int {
	avg := make([]int, len(nums))
	prefix := generatePrefix(nums)

	for i := 0; i < len(nums); i++ {
		if !isSafe(i, k, len(nums)) {
			avg[i] = -1
			continue
		}
		//avg[i] = getAverage(nums, i, k)
		avg[i] = getAveragePrefix(prefix, i, k)
	}
	return avg
}

func main() {
	num := []int{7, 4, 3, 9, 1, 8, 5, 2, 6}
	fmt.Println(getAverages(num, 3))
}
