// There is an integer array nums sorted in ascending order (with distinct values).
//
// Prior to being passed to your function, nums is possibly rotated at an unknown pivot index k (1 <= k < nums.length) such that the resulting array is [nums[k], nums[k+1], ..., nums[n-1], nums[0], nums[1], ..., nums[k-1]] (0-indexed). For example, [0,1,2,4,5,6,7] might be rotated at pivot index 3 and become [4,5,6,7,0,1,2].
//
// Given the array nums after the possible rotation and an integer target, return the index of target if it is in nums, or -1 if it is not in nums.
//
// You must write an algorithm with O(log n) runtime complexity.

package main

func search(nums []int, target int) int {
	point := findDiffPoint(nums, 0, len(nums)-1)
	var ans int
	if point == -1 {
		return bsearch(nums, 0, len(nums)-1, target)
	}
	ans = bsearch(nums, 0, point, target)
	if ans == -1 {
		return bsearch(nums, point+1, len(nums)-1, target)
	}

	return ans
}

func bsearch(arr []int, left, right int, target int) int {
	if left <= right {
		mid := left + (right-left)/2

		if arr[mid] == target {
			return mid
		}
		if arr[mid] < target {
			return bsearch(arr, mid+1, right, target)
		} else {
			return bsearch(arr, left, mid-1, target)
		}
	}
	return -1
}

func findDiffPoint(arr []int, left, right int) int {
	s := 0
	e := len(arr) - 1
	if left <= right {
		mid := left + (right-left)/2
		if mid < e && arr[mid] > arr[mid+1] {
			return mid
		}
		if s < mid && arr[mid-1] > arr[mid] {
			return mid - 1
		}
		if arr[mid] > arr[s] {
			return findDiffPoint(arr, mid+1, right)
		} else {
			return findDiffPoint(arr, left, mid-1)
		}
	}
	return -1
}
