// You are given an m x n integer array grid. There is a robot initially located at the top-left corner (i.e., grid[0][0]). The robot tries to move to the bottom-right corner (i.e., grid[m - 1][n - 1]). The robot can only move either down or right at any point in time.
//
// An obstacle and space are marked as 1 or 0 respectively in grid. A path that the robot takes cannot include any square that is an obstacle.
//
// Return the number of possible unique paths that the robot can take to reach the bottom-right corner.
//
// The testcases are generated so that the answer will be less than or equal to 2 * 109.
package main

func uniquePathsWithObstacles(arr [][]int) int {
	if arr[0][0] == 1 {
		return 0
	}
	memory := make([][]int, len(arr))
	for i := range arr {
		memory[i] = make([]int, len(arr[0]))
		for j := 0; j < len(arr[0]); j++ {
			memory[i][j] = -1
		}
	}
	return eval(len(arr)-1, len(arr[0])-1, arr, memory)
}

func eval(m, n int, obstacle [][]int, mem [][]int) int {
	if m < 0 || n < 0 {
		return 0
	}
	if mem[m][n] != -1 {
		return mem[m][n]
	}

	if m == 0 && n == 0 {
		return 1
	}
	if obstacle[m][n] == 1 {
		return 0
	}
	val := eval(m-1, n, obstacle, mem) + eval(m, n-1, obstacle, mem)
	mem[m][n] = val
	return val
}
