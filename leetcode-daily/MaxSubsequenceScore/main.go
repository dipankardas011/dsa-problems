// You are given two 0-indexed integer arrays nums1 and nums2 of equal length n and a positive integer k. You must choose a subsequence of indices from nums1 of length k.

// For chosen indices i0, i1, ..., ik - 1, your score is defined as:

// The sum of the selected elements from nums1 multiplied with the minimum of the selected elements from nums2.
// It can defined simply as: (nums1[i0] + nums1[i1] +...+ nums1[ik - 1]) * min(nums2[i0] , nums2[i1], ... ,nums2[ik - 1]).
// Return the maximum possible score.

// A subsequence of indices of an array is a set that can be derived from the set {0, 1, ..., n-1} by deleting some or no elements.
package main

import (
	"container/heap"
	"sort"
)

type IntHeap []int

func (h IntHeap) Len() int           { return len(h) }
func (h IntHeap) Less(i, j int) bool { return h[i] < h[j] }
func (h IntHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *IntHeap) Push(x interface{}) {
	*h = append(*h, x.(int))
}

func (h *IntHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

func maxScore(nums1 []int, nums2 []int, k int) int64 {
	n := len(nums1)
	ess := make([][]int, n)
	for i := 0; i < n; i++ {
		ess[i] = []int{nums2[i], nums1[i]}
	}
	sort.Slice(ess, func(i, j int) bool {
		return ess[i][0] > ess[j][0]
	})

	pq := &IntHeap{}
	heap.Init(pq)

	var res, sumS int64 = 0, 0
	for _, es := range ess {
		heap.Push(pq, es[1])
		sumS += int64(es[1])
		if pq.Len() > k {
			sumS -= int64(heap.Pop(pq).(int))
		}
		if pq.Len() == k {
			temp := sumS * int64(es[0])
			if temp > res {
				res = temp
			}
		}
	}
	return res
}

// func main() {
// 	nums1 := []int{14, 2, 1, 12}
// 	nums2 := []int{13, 11, 7, 6}
// 	k := 3
// 	fmt.Println(maxScore(nums1, nums2, k))
// }
