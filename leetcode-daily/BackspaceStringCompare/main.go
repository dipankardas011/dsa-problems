package main

import (
	"errors"
	"fmt"
	"strings"
)

type node struct {
	Val  rune
	Next *node
}

type Stack struct {
	head *node
}

func (stk *Stack) Push(ch rune) {
	temp := new(node)
	temp.Val = ch
	temp.Next = stk.head
	stk.head = temp
}

func (stk *Stack) Pop() (rune, error) {
	if stk.head == nil {
		return ' ', errors.New("its already empty")
	}
	temp := stk.head
	stk.head = stk.head.Next
	return temp.Val, nil
}

func (stk *Stack) Len() int {
	temp := stk.head
	length := 0
	for temp != nil {
		length++
		temp = temp.Next
	}
	return length
}

func extractNewString(s string) string {
	stack := new(Stack)
	for _, c := range s {
		if c == '#' {
			_, _ = stack.Pop()
		} else {
			// push
			stack.Push(c)
		}
	}
	var ret strings.Builder
	for stack.Len() > 0 {
		c, err := stack.Pop()
		if err == nil {
			ret.WriteRune(c)
		}
	}
	return ret.String()
}

func backspaceCompare(s string, t string) bool {
	return extractNewString(s) == extractNewString(t)
}

func main() {
	fmt.Println(backspaceCompare("ab#c", "ad#c"))
	fmt.Println(backspaceCompare("ab##", "c#d#"))
	fmt.Println(backspaceCompare("a#c", "b"))
}
