// Given an array of integers nums containing n + 1 integers where each integer is in the range [1, n] inclusive.
//
// There is only one repeated number in nums, return this repeated number.
//
// You must solve the problem without modifying the array nums and uses only constant extra space.
package main

import "fmt"

func findDuplicate(nums []int) int {
	for i := 0; i < len(nums); {
		if i+1 != nums[i] {
			if nums[i] != nums[nums[i]-1] {
				t := nums[nums[i]-1]
				nums[nums[i]-1] = nums[i]
				nums[i] = t
			} else {
				// idx like   { 3 4 }
				// values are { 4 4 }
				// when we are at idx == 4
				// duplicate
				return nums[i]
			}
		} else {
			// in order
			// i => 4
			// num[i] => 5
			i++
		}
	}
	return -1
}

func main() {
	fmt.Println(findDuplicate([]int{1, 3, 4, 2, 2}))
}
