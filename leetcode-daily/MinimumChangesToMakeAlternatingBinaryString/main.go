// You are given a string s consisting only of the characters '0' and '1'. In one operation, you can change any '0' to '1' or vice versa.
//
// The string is called alternating if no two adjacent characters are equal. For example, the string "010" is alternating, while the string "0100" is not.
//
// Return the minimum number of operations needed to make s alternating.
package main

import (
	"fmt"
)

func minOperations(s string) int {
	cntStart1, cntStart0 := 0, 0

	incTerm := func(t rune) rune {
		if t == '1' {
			return '0'
		}
		return '1'
	}

	term := '1'
	for _, r := range s {

		if r != term {
			cntStart1++
		}

		term = incTerm(term)
	}

	term = '0'
	for _, r := range s {

		if r != term {
			cntStart0++
		}

		term = incTerm(term)
	}

	return min(cntStart0, cntStart1)
}

func main() {
	fmt.Println(minOperations("0100"))
}
