// You are given an integer array matches where matches[i] = [winneri, loseri] indicates that the player winneri defeated player loseri in a match.
//
// Return a list answer of size 2 where:
//
// answer[0] is a list of all players that have not lost any matches.
// answer[1] is a list of all players that have lost exactly one match.
// The values in the two lists should be returned in increasing order.
//
// Note:
//
// You should only consider the players that have played at least one match.
// The testcases will be generated such that no two matches will have the same outcome.

package main

import "slices"

func findWinners(matches [][]int) [][]int {
	counter := make(map[int]int)
	for _, match := range matches {
		winner := match[0]
		looser := match[1]
		if v, ok := counter[looser]; !ok {
			counter[looser] = 1
		} else {
			counter[looser] = v + 1
		}
		if _, ok := counter[winner]; !ok {
			counter[winner] = 0
		}
	}
	res := make([][]int, 2)
	for k, v := range counter {

		if v == 0 || v == 1 {
			res[v] = append(res[v], k)
		}
	}
	slices.Sort(res[0])
	slices.Sort(res[1])
	return res
}
