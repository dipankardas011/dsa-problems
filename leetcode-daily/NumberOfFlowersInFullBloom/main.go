// You are given a 0-indexed 2D integer array flowers, where flowers[i] = [starti, endi] means the ith flower will be in full bloom from starti to endi (inclusive). You are also given a 0-indexed integer array people of size n, where people[i] is the time that the ith person will arrive to see the flowers.

// Return an integer array answer of size n, where answer[i] is the number of flowers that are in full bloom when the ith person arrives.
package main

import (
	"fmt"
	"sort"
)

func fullBloomFlowers(flowers [][]int, people []int) []int {
	start := make([]int, len(flowers))
	end := make([]int, len(flowers))
	// Note: that a flower = [start, end] stops blooming at end + 1, not end. There are two ways we can handle this. We can either binary search on end for the leftmost insertion index (since we want to include all flowers with end equal to the current time), or we can assemble ends using end + 1 for each flower
	// https://leetcode.com/problems/number-of-flowers-in-full-bloom/editorial/
	for i, f := range flowers {
		start[i] = f[0]
		end[i] = f[1] + 1
	}

	sort.Ints(start)
	sort.Ints(end)

	var flowerList []int
	for _, person := range people {
		i := binarySearch(start, person)
		j := binarySearch(end, person)
		flowerList = append(flowerList, i-j)
	}
	return flowerList
}

func binarySearch(arr []int, target int) int {
	left := 0
	right := len(arr)
	for left < right {
		mid := (left + right) / 2
		if target < arr[mid] {
			right = mid
		} else {
			left = mid + 1
		}
	}

	return left
}

func main() {
	fmt.Printf("%+v", fullBloomFlowers([][]int{{1, 10}, {3, 3}}, []int{3, 3, 2}))
}
