package main

func longestSubarray(nums []int) int {
	zeroCount := 0

	maxLen := 0

	startIdx := 0

	for idx, num := range nums {
		if num == 0 {
			zeroCount++
		}
		for zeroCount > 1 {
			if nums[startIdx] == 0 {
				zeroCount--
			}
			startIdx++
		}

		currLen := idx - startIdx + 1 - 1 // for excluding the zero from len counting
		if maxLen < currLen {
			maxLen = currLen
		}
	}

	return maxLen
}
