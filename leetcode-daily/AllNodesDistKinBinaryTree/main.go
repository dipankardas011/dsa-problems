package main

type Set struct {
	data map[int]bool
}

// allocates the memory to the Set
func (m *Set) Create() {
	m.data = make(map[int]bool, 0)
}

// adds elements of comparable type <Generics>
func (m *Set) Add(key int) {
	m.data[key] = true
}

type Graph map[int][]int

func distanceK(root *TreeNode, target *TreeNode, k int) []int {
	targetVertex := target.Val
	graph := make(Graph, 0)
	ret := make([]int, 0)

	var isVisited Set = Set{}
	isVisited.Create()

	buildGraph(&graph, root, nil)

	isVisited.Add(targetVertex)
	dfs(targetVertex, graph, 0, k, isVisited, &ret)
	return ret
}

func dfs(curr int, g Graph, step, k int, isVisited Set, ans *[]int) {
	if step == k {
		(*ans) = append((*ans), curr)
	}

	neighbour, _ := g[curr]
	for _, child := range neighbour {
		if !isVisited.data[child] {
			isVisited.Add(child)
			dfs(child, g, step+1, k, isVisited, ans)
		}
	}
}

func buildGraph(g *Graph, curr *TreeNode, parent *TreeNode) {
	if curr != nil && parent != nil {
		(*g)[curr.Val] = append((*g)[curr.Val], parent.Val)

		(*g)[parent.Val] = append((*g)[parent.Val], curr.Val)
	}

	if parent == nil && curr.Left == nil && curr.Right == nil {
		// only root TreeNode
		(*g)[curr.Val] = make([]int, 0)
	}
	if curr.Left != nil {
		buildGraph(g, curr.Left, curr)
	}
	if curr.Right != nil {
		buildGraph(g, curr.Right, curr)
	}

}
