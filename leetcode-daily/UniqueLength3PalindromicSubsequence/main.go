// Given a string s, return the number of unique palindromes of length three that are a subsequence of s.
//
// Note that even if there are multiple ways to obtain the same subsequence, it is still only counted once.
//
// A palindrome is a string that reads the same forwards and backwards.
//
// A subsequence of a string is a new string generated from the original string with some characters (can be none) deleted without changing the relative order of the remaining characters.
//
// For example, "ace" is a subsequence of "abcde".

package main

import "fmt"

type HashSet struct {
	set map[rune]any
}

func NewHashMap() *HashSet {
	hs := new(HashSet)
	hs.set = make(map[rune]any)
	return hs
}

func (hs *HashSet) Add(k rune) {
	hs.set[k] = nil
}

func (hs *HashSet) Len() int {
	return len(hs.set)
}

func (hs *HashSet) Find(k rune) (found bool) {
	_, found = hs.set[k]
	return
}

func (hs *HashSet) iter() []rune {
	var keys []rune

	for k, _ := range hs.set {
		keys = append(keys, k)
	}

	return keys
}

func countPalindromicSubsequence(s string) int {

	ans := 0

	letters := NewHashMap()
	for _, c := range s {
		letters.Add(c)
	}

	for _, letter := range letters.iter() {
		i := -1
		j := 0

		for k, c := range s {
			if c == letter {
				if i == -1 {
					i = k
				}

				j = k
			}
		}
		between := NewHashMap()
		for k := i + 1; k < j; k++ {
			between.Add(rune(s[k]))
		}
		ans += between.Len()
	}

	return ans
}

func main() {
	fmt.Println(countPalindromicSubsequence("aabca"))
}
