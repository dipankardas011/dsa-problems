// You are given an m x n integer matrix grid, where you can move from a cell to any adjacent cell in all 4 directions.

// Return the number of strictly increasing paths in the grid such that you can start from any cell and end at any cell. Since the answer may be very large, return it modulo 109 + 7.

// Two paths are considered different if they do not have exactly the same sequence of visited cells.
package main

import "fmt"

const MOD_INT = 1000000007

var dirs [][2]int = [][2]int{{0, -1}, {0, 1}, {1, 0}, {-1, 0}}

func isValid(i, j, m, n int) bool {
	if i >= 0 && j >= 0 &&
		i < m && j < n {
		return true
	}
	return false
}

func dp(grid, mem [][]int, i, j int) int {
	m := len(grid)
	n := len(grid[0])
	// if !isValid(i, j, m, n) {
	// 	return 0
	// }
	adder := 1
	if mem[i][j] != 0 {
		return mem[i][j]
	}

	for _, dir := range dirs {
		I := i + dir[0]
		J := j + dir[1]

		if isValid(I, J, m, n) && grid[I][J] < grid[i][j] {
			adder = (adder + dp(grid, mem, I, J)) % MOD_INT
		}
	}

	mem[i][j] = adder
	return adder
}

func countPaths(grid [][]int) int {
	m := len(grid)
	n := len(grid[0])
	var mem [][]int = make([][]int, m)

	for i := 0; i < m; i++ {
		mem[i] = make([]int, n)
	}

	result := 0

	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			result = (result + dp(grid, mem, i, j)) % MOD_INT
		}
	}
	return result
}

func main() {
	grid := [][]int{
		{1, 1},
		{3, 4},
	}
	fmt.Println(countPaths(grid))
}
