package main

import (
	"fmt"
	"strconv"
	"strings"
	"unicode"
)

func decodeAtIndex(s string, k int) string {
	var decodeLen uint64
	var ch rune

	for i := 0; i < len(s); i++ {
		ch = rune(s[i])

		if unicode.IsDigit(ch) {
			decodeLen *= uint64(ch - '0')
		} else {
			decodeLen++
		}
	}

	for i := len(s) - 1; i >= 0; i-- {
		ch = rune(s[i])

		if unicode.IsDigit(ch) {
			decodeLen /= uint64(ch - '0')
			k %= int(decodeLen) // to scale the k to current len
		} else {
			if k == 0 || k == int(decodeLen) {
				return string(ch)
			}
			decodeLen--
		}
	}

	return ""
}

func decodeString(s string, k int) string {
	// generate the string upto the required kth index
	var ret strings.Builder

	for i := 0; i < len(s) && ret.Len() <= k; i++ {
		if unicode.IsDigit(rune(s[i])) {
			// digits
			no, err := strconv.Atoi(string(s[i]))
			if ret.Len()*(no-1) > k {
				break
			}
			if err != nil {
				panic(err)
			}
			ret.WriteString(strings.Repeat(ret.String(), no-1))
		} else {
			// imply add
			ret.WriteByte(s[i])
		}
	}

	return string(ret.String()[k-1])
}

func main() {
	fmt.Println(decodeAtIndex("leet2code3", 10))
	fmt.Println(decodeAtIndex("ha22", 5))
	fmt.Println(decodeAtIndex("a2345678999999999999999", 1))
}
