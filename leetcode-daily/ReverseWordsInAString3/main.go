package main

import (
	"fmt"
	"strings"
)

func reverseWords(s string) string {
	words := strings.Split(s, " ")
	res := ""
	for _, word := range words {

		rawWord := []rune(word)
		for left, right := 0, len(word)-1; left < right; left, right = left+1, right-1 {
			rawWord[left], rawWord[right] = rawWord[right], rawWord[left]
		}
		res += string(rawWord) + " "
	}
	return strings.Trim(res, " ")
}

func main() {
	fmt.Println(reverseWords("Let's take LeetCode contest"))
}
