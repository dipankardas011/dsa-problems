package main

import (
	"fmt"
	"sort"
)

type different []string

// Len implements sort.Interface.
func (d different) Len() int {
	return len(d)
}

// Less implements sort.Interface.
func (d different) Less(i int, j int) bool {
	return len(d[i]) < len(d[j])
}

// Swap implements sort.Interface.
func (d different) Swap(i int, j int) {
	d[i], d[j] = d[j], d[i]
}

func longestStrChain(words []string) int {

	rawWords := different(words)
	sort.Sort(rawWords)

	hsMap := make(map[string]int)

	res := 0
	for _, word := range []string(rawWords) {
		count := 0
		for i := 0; i < len(word); i++ {
			checkString := word[0:i] + word[i+1:]
			v, _ := hsMap[checkString] // no need to check for default value as by default if not found returns 0
			count = max(count, v+1)
		}

		hsMap[word] = count
		res = max(res, count)
	}
	return res
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func main() {
	fmt.Println(longestStrChain([]string{"a", "b", "ba", "bca", "bda", "bdca"}))
}
