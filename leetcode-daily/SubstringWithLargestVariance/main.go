// The variance of a string is defined as the largest difference between the number of occurrences of any 2 characters present in the string. Note the two characters may or may not be the same.

// Given a string s consisting of lowercase English letters only, return the largest variance possible among all substrings of s.

// A substring is a contiguous sequence of characters within a string.

package main

func calcVariance(s string) int {
	freq := make([]int, 26)

	min := len(s) + 1
	max := 0
	for _, char := range s {
		i := char - 'a'
		freq[i] += 1
	}
	for i := 0; i <= 25; i++ {
		if freq[i] == 0 {
			continue
		}
		if freq[i] > max {
			max = freq[i]
		}
		if freq[i] < min {
			min = freq[i]
		}
	}
	return max - min
}

func generateSubstrings(input string) []string {
	substrings := []string{}
	for i := 0; i < len(input); i++ {
		for j := i + 1; j <= len(input); j++ {
			substring := input[i:j]
			substrings = append(substrings, substring)
		}
	}
	return substrings
}

// resulting in 119 / 138 testcases passed
// problem is to calculate the substring
// func largestVariance(s string) int {
// 	if len(s) <= 1 {
// 		return 0
// 	}
// 	mem := make(map[string]int, 0)

// 	max := 0

// 	substrings := generateSubstrings(s)

// 	for _, substring := range substrings {
// 		cache, present := mem[substring]

// 		if !present {
// 			// no present
// 			cache = calcVariance(substring)
// 			mem[substring] = cache
// 		}

// 		if cache > max {
// 			max = cache
// 		}
// 	}
// 	return max
// }

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

// https://leetcode.com/problems/substring-with-largest-variance/editorial/
// Kadane's Algorithm
func largestVariance(s string) int {
	freq := make([]int, 26)
	for _, ch := range s {
		freq[int(ch-'a')] += 1
	}
	globalMax := 0

	for i := 0; i < 26; i++ {
		for j := 0; j < 26; j++ {
			if i == j || freq[i] == 0 || freq[j] == 0 {
				continue
			}

			major := rune('a' + i)
			minor := rune('a' + j)
			majorCnt := 0
			minorCnt := 0

			restOfMinor := freq[j]

			for _, ch := range s {
				if ch == major {
					majorCnt++
				}
				if ch == minor {
					minorCnt++
					restOfMinor--
				}

				if minorCnt > 0 {
					globalMax = max(globalMax, majorCnt-minorCnt)
				}

				if majorCnt < minorCnt && restOfMinor > 0 {
					majorCnt = 0
					minorCnt = 0
				}
			}
		}
	}
	return globalMax
}
