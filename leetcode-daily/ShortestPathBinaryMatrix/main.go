package main

import (
	"container/list"
	"fmt"
)

type QueueStore struct {
	i int
	j int
	d int
}

func shortestPathBinaryMatrix(grid [][]int) int {
	queue := list.New()
	n := len(grid)
	if grid[0][0] == 1 || grid[n-1][n-1] == 1 {
		return -1
	}
	dirChg := [][]int{
		{1, 0},
		{-1, 0},
		{0, 1},
		{0, -1},
		{1, 1},
		{-1, -1},
		{1, -1},
		{-1, 1},
	}
	queue.PushBack(QueueStore{i: 0, j: 0, d: 1})

	for queue.Len() > 0 {
		rm := queue.Front()
		src := rm.Value.(QueueStore)
		queue.Remove(rm)
		if src.i == n-1 && src.j == n-1 {
			return src.d
		}

		for k := 0; k < len(dirChg); k++ {
			i, j := dirChg[k][0]+src.i, dirChg[k][1]+src.j
			if i >= 0 && i < n && j >= 0 && j < n && grid[i][j] == 0 {
				queue.PushBack(QueueStore{i: i, j: j, d: 1 + src.d})
				grid[i][j] = 1
			}
		}
	}
	return -1
}

func main() {
	grid := [][]int{{0, 0, 0}, {1, 1, 0}, {1, 1, 0}}
	fmt.Println(shortestPathBinaryMatrix(grid))
}
