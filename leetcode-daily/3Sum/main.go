package main

import "sort"

func threeSum(nums []int) [][]int {
	sort.Ints(nums)
	hs := make(map[int]int, 0)
	for idx, num := range nums {
		hs[num] = idx
	}
	var res [][]int = make([][]int, 0)
	for i := 0; i < len(nums)-2; i++ {
		for j := i + 1; j < len(nums)-1; j++ {
			target := 0 - nums[i] - nums[j]
			if k, ok := hs[target]; ok && j < k {
				res = append(res, []int{nums[i], nums[j], target})
				j, _ = hs[nums[j]]
			}
			i, _ = hs[nums[i]]
		}
	}
	return res
}
