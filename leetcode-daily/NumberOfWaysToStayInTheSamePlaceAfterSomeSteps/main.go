package main

import "fmt"

const MOD = 1e9 + 7

func min(a, b int) int {
	if a > b {
		return b
	}
	return a
}

func numWays(steps int, arrLen int) int {
	arrLen = min(steps, arrLen)
	dp := make([][]int, arrLen)
	for i := range dp {
		dp[i] = make([]int, steps+1)
		for j := 0; j < steps+1; j++ {
			dp[i][j] = -1
		}
	}
	return execDP(0, steps, arrLen, dp)
	// return execNoDP(0, steps, arrLen)
}

func execDP(curr, remainingSteps, arrLen int, dp [][]int) int {

	if remainingSteps == 0 {
		if curr == 0 {
			return 1
		}
		return 0
	}

	if dp[curr][remainingSteps] != -1 {
		return dp[curr][remainingSteps]
	}

	ans := execDP(curr, remainingSteps-1, arrLen, dp)
	if curr > 0 {
		ans = (ans + execDP(curr-1, remainingSteps-1, arrLen, dp)) % MOD
	}
	if curr < arrLen-1 {
		ans = (ans + execDP(curr+1, remainingSteps-1, arrLen, dp)) % MOD
	}

	dp[curr][remainingSteps] = ans
	return ans
}

//
// func execNoDP(curr, remainingSteps int, arrLen int) int {
// 	if curr < 0 || curr == arrLen {
// 		return 0
// 	}
//
// 	if remainingSteps == 0 {
// 		if curr != 0 {
// 			return 0
// 		}
// 		return 1
// 	}
//
// 	return execNoDP(curr+1, remainingSteps-1, arrLen) + execNoDP(curr, remainingSteps-1, arrLen) + execNoDP(curr-1, remainingSteps-1, arrLen)
// }

func main() {
	fmt.Println(numWays(3, 2))
	fmt.Println(numWays(2, 4))
	fmt.Println(numWays(4, 2))
}
