// s1	=	(0,	0,	0,	0,	0,	0,	0,	...)
// s2	=	(1,	1,	1,	1,	1,	1,	1,	...)
// s3	=	(0,	1,	0,	1,	0,	1,	0,	...)
// s4	=	(1,	0,	1,	0,	1,	0,	1,	...)
// s5	=	(1,	1,	0,	1,	0,	1,	1,	...)
// s6	=	(0,	0,	1,	1,	0,	1,	1,	...)
// s7	=	(1,	0,	0,	0,	1,	0,	0,	...)
// ...
// s	=	(1,	0,	1,	1,	1,	0,	1,	...)
// Next, a sequence s is constructed by choosing the 1st digit as complementary to the 1st digit of s1 (swapping 0s for 1s and vice versa), the 2nd digit as complementary to the 2nd digit of s2, the 3rd digit as complementary to the 3rd digit of s3, and generally for every n, the nth digit as complementary to the nth digit of sn. For the example above, this yields

package main

import (
	"fmt"
	"strconv"
	"strings"
)

type HashSet[T comparable] struct {
	set map[T]any
}

func NewHashSet[T comparable]() *HashSet[T] {
	hs := new(HashSet[T])
	hs.set = make(map[T]any)
	return hs
}

func (hs *HashSet[T]) Add(k T) {
	hs.set[k] = nil
}

func (hs *HashSet[T]) Len() int {
	return len(hs.set)
}

func (hs *HashSet[T]) Find(k T) (found bool) {
	_, found = hs.set[k]
	return
}

func (hs *HashSet[T]) iter() []T {
	var keys []T

	for k, _ := range hs.set {
		keys = append(keys, k)
	}

	return keys
}

func usingHashSet(nums []string) string {
	hs := NewHashSet[int64]()
	for _, num := range nums {
		i, _ := strconv.ParseInt(num, 2, 64)
		hs.Add(i)
	}

	n := len(nums)

	for num := 0; num <= n; num++ {
		if !hs.Find(int64(num)) {
			bin := strconv.FormatInt(int64(num), 2)
			for len(bin) < n {
				bin = "0" + bin
			}
			return bin
		}
	}
	return ""
}

func usingCantorsDiagonal(nums []string) string {

	var ret strings.Builder
	for i, num := range nums {
		current := rune(num[i])
		if current == '0' {
			ret.WriteRune('1')
		} else {
			ret.WriteRune('0')
		}
	}
	return ret.String()
}

func findDifferentBinaryString(nums []string) string {
	return usingHashSet(nums)
	// return usingCantorsDiagonal(nums)
}

func main() {
	fmt.Println(findDifferentBinaryString([]string{"01", "00"}))
}
