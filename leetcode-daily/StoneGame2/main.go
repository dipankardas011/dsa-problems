// Alice and Bob continue their games with piles of stones.  There are a number of piles arranged in a row, and each pile has a positive integer number of stones piles[i].  The objective of the game is to end with the most stones.

// Alice and Bob take turns, with Alice starting first.  Initially, M = 1.

// On each player's turn, that player can take all the stones in the first X remaining piles, where 1 <= X <= 2M.  Then, we set M = max(M, X).

// The game continues until all the stones have been taken.

// Assuming Alice and Bob play optimally, return the maximum number of stones Alice can get.
package main

import (
	"fmt"
	"math"
)

func main() {
	pile := []int{2, 7, 9, 4, 4}
	fmt.Println(stoneGameII(pile))
}

func util_stone_2_dp(dp [][][]int, piles []int, idx int, turn int, m int) int {
	if idx == len(piles) {
		return 0
	}

	if dp[turn][idx][m] != -1 {
		return dp[turn][idx][m]
	}

	ret := 0
	count := 0
	if turn == 0 {
		ret = -1
	} else {
		ret = 1000000
	}

	for x := 1; x <= int(math.Min(float64(2*m), float64(len(piles)-idx))); x++ {
		count += piles[idx+x-1]
		M := int(math.Max(float64(m), float64(x)))
		getRet := util_stone_2_dp(dp, piles, idx+x, (turn+1)%2, M)

		if turn == 0 {
			// for the alias
			ret = int(math.Max(float64(count+getRet), float64(ret)))
		} else {
			// for the bob
			ret = int(math.Min(float64(getRet), float64(ret)))
		}
	}

	dp[turn][idx][m] = ret

	return ret
}
func util_stone_2(piles []int, idx int, turn bool, m int) int {
	if idx == len(piles) {
		return 0
	}

	ret := 0
	count := 0
	if turn {
		ret = -1
	} else {
		ret = 1000000
	}

	for x := 1; x <= int(math.Min(float64(2*m), float64(len(piles)-idx))); x++ {
		count += piles[idx+x-1]
		M := int(math.Max(float64(m), float64(x)))
		getRet := util_stone_2(piles, idx+x, !turn, M)

		if turn {
			// for the alias
			ret = int(math.Max(float64(count+getRet), float64(ret)))
		} else {
			// for the bob
			ret = int(math.Min(float64(getRet), float64(ret)))
		}
	}

	return ret
}

func stoneGameII(piles []int) int {
	dp := make([][][]int, 2)
	for a := 0; a < 2; a++ {
		dp[a] = make([][]int, len(piles)+1)
		for i := 0; i <= len(piles); i++ {
			dp[a][i] = make([]int, len(piles)+1)
		}
	}
	for i := 0; i < 2; i++ {
		for j := 0; j <= len(piles); j++ {
			for k := 0; k <= len(piles); k++ {
				dp[i][j][k] = -1
			}
		}
	}
	return util_stone_2_dp(dp, piles, 0, 0, 1)
}
