// Given 3 positives numbers a, b and c. Return the minimum flips required in some bits of a and b to make ( a OR b == c ). (bitwise OR operation).
// Flip operation consists of change any single bit 1 to 0 or change the bit 0 to 1 in their binary representation.
package main

func minFlips(a int, b int, c int) int {
	ret := 0

	for a > 0 || b > 0 || c > 0 {
		l_a := a & 1
		l_b := b & 1
		l_c := c & 1
		if l_a|l_b != l_c {
			if l_c&1 == 1 {
				ret++ // make anyone of the bit as 1
			} else {
				ret += l_a + l_b // minimum 1 bit need to be flipped and max 2
			}
		}
		a >>= 1
		b >>= 1
		c >>= 1
	}
	return ret
}
