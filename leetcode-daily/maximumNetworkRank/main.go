// There is an infrastructure of n cities with some number of roads connecting these cities. Each roads[i] = [ai, bi] indicates that there is a bidirectional road between cities ai and bi.
//
// The network rank of two different cities is defined as the total number of directly connected roads to either city. If a road is directly connected to both cities, it is only counted once.
//
// The maximal network rank of the infrastructure is the maximum network rank of all pairs of different cities.
//
// Given the integer n and the array roads, return the maximal network rank of the entire infrastructure.
package main

func maximalNetworkRank(n int, roads [][]int) int {
	graph := make([][]int, n)

	mem := make(map[string]bool, 0)
	// dont add anything
	for _, road := range roads {
		road_x := road[0]
		road_y := road[1]

		inter_1 := string(road_x) + " " + string(road_y)
		inter_2 := string(road_y) + " " + string(road_x)
		mem[inter_1] = true
		mem[inter_2] = true

		graph[road_x] = append(graph[road_x], road_y)
		graph[road_y] = append(graph[road_y], road_x)
	}
	max := 0
	for v1 := 0; v1 < n; v1++ {
		for v2 := v1 + 1; v2 < n; v2++ {
			count := len(graph[v1]) + len(graph[v2])
			inter := string(v1) + " " + string(v2)
			if _, ok := mem[inter]; ok {
				count--
			}
			if max < count {
				max = count
			}
		}
	}
	return max
}
