package main

func search(nums []int, target int) bool {
	point := findDiffPointDup(nums, 0, len(nums)-1)
	var ans bool

	if point == -1 {
		return bsearch(nums, 0, len(nums)-1, target)
	}

	ans = bsearch(nums, 0, point, target)
	if !ans {
		return bsearch(nums, point+1, len(nums)-1, target)
	}

	return ans
}

func bsearch(arr []int, left, right int, target int) bool {
	if left <= right {
		mid := left + (right-left)/2

		if arr[mid] == target {
			return true
		}
		if arr[mid] < target {
			return bsearch(arr, mid+1, right, target)
		} else {
			return bsearch(arr, left, mid-1, target)
		}
	}
	return false
}

func findDiffPointDup(arr []int, left, right int) int {
	s := left
	e := right
	if left <= right {
		mid := left + (right-left)/2
		if mid < e && arr[mid] > arr[mid+1] {
			return mid
		}
		if s < mid && arr[mid-1] > arr[mid] {
			return mid - 1
		}

		if arr[s] == arr[mid] && arr[e] == arr[mid] {
			for s < e && arr[s] == arr[s+1] {
				s++
			}
			if s < e && arr[s] > arr[s+1] {
				return s
			}
			for s < e && arr[e] == arr[e-1] {
				e--
			}
			if s < e && arr[e-1] > arr[e] {
				return e - 1
			}
		} else if arr[mid] > arr[s] || (arr[s] == arr[mid] && arr[mid] > arr[e]) {
			return findDiffPointDup(arr, mid+1, e)
		} else {
			return findDiffPointDup(arr, s, mid-1)
		}
	}
	return -1
}
