// Given a wooden stick of length n units. The stick is labelled from 0 to n. For example, a stick of length 6 is labelled as follows:

// Given an integer array cuts where cuts[i] denotes a position you should perform a cut at.

// You should perform the cuts in order, you can change the order of the cuts as you wish.

// The cost of one cut is the length of the stick to be cut, the total cost is the sum of costs of all cuts. When you cut a stick, it will be split into two smaller sticks (i.e. the sum of their lengths is the length of the stick before the cut). Please refer to the first example for a better explanation.

// Return the minimum total cost of the cuts.
package main

import (
	"math"
	"sort"
)

func utilCost(cuts []int, dp [][]int, i, j int) int {
	if j-i <= 1 {
		return 0
	}
	if dp[i][j] == 0 {
		dp[i][j] = math.MaxInt32

		for k := i + 1; k < j; k++ {
			leftRecurse := utilCost(cuts, dp, i, k)
			rightRecurse := utilCost(cuts, dp, k, j)

			cost := cuts[j] - cuts[i] + leftRecurse + rightRecurse

			dp[i][j] = int(math.Min(float64(cost), float64(dp[i][j])))

		}
	}
	return dp[i][j]
}

func minCost(n int, cuts []int) int {
	cuts = append(cuts, 0)
	cuts = append(cuts, n)
	sort.Ints(cuts)

	dp := make([][]int, 102)
	for i := range dp {
		dp[i] = make([]int, 102)
	}

	return utilCost(cuts, dp, 0, len(cuts)-1)
}
