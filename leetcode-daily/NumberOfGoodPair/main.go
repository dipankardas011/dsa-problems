// Given an array of integers nums, return the number of good pairs.
//
// A pair (i, j) is called good if nums[i] == nums[j] and i < j.
package main

import "fmt"

func numIdenticalPairs(nums []int) int {
	count := make([]int, 101)

	for _, num := range nums {
		count[num]++
	}
	res := 0
	for _, n := range count {
		res += (n * (n - 1)) / 2 // no of pairs being calculated, combinational formula when n elements can happen n-1 times
	}
	return res
}

func main() {
	fmt.Println(numIdenticalPairs([]int{1, 2, 3, 1, 1, 3}))
}
