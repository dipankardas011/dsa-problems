package main

import "fmt"

func main() {
	arr := []int{3, 8, 7, 6, -2, -8, 4, 9}
	sgtree := &SegmentTree{}
	sgtree.ConstructTree(arr)

	sgtree.DisplayTree()

	fmt.Println("Sum [2,6]: ", sgtree.GetSum(2, 6))

	sgtree.UpdateIndex(3, 14)

	sgtree.DisplayTree()
}
