package main

import (
	"fmt"
)

type Interval struct {
	X int
	Y int
}

type Node struct {
	Query int
	Interval

	Left  *Node
	Right *Node
}

type SegmentTree struct {
	Root *Node
}

func (obj *SegmentTree) ConstructTree(arr []int) {
	obj.Root = makeTree(arr, 0, len(arr)-1)
}

func makeTree(arr []int, l, h int) *Node {
	if l == h {
		return &Node{
			Query:    arr[l],
			Interval: Interval{l, h},
			Left:     nil, Right: nil}
	}
	mid := l + (h-l)/2
	nodeLeft := makeTree(arr, l, mid)
	nodeRight := makeTree(arr, mid+1, h)
	return &Node{
		Query:    nodeLeft.Query + nodeRight.Query,
		Interval: Interval{l, h},
		Left:     nodeLeft,
		Right:    nodeRight,
	}
}

func (obj *SegmentTree) GetSum(left, right int) int {
	var calSum func(*Node, int, int) int

	root := obj.Root

	calSum = func(node *Node, left, right int) int {
		if right < node.Interval.X || node.Interval.Y < left {
			return 0
		}

		if left <= node.Interval.X && node.Interval.Y <= right {
			return node.Query
		}
		return calSum(node.Left, left, right) + calSum(node.Right, left, right)
	}

	return calSum(root, left, right)
}

func (obj *SegmentTree) UpdateIndex(index, value int) {
	var updateSum func(*Node, int, int) int

	updateSum = func(n *Node, idx, v int) int {
		if n.Interval.X <= idx && idx <= n.Interval.Y {
			if n.Interval.X == idx && n.Interval.Y == idx {
				n.Query = v
				return n.Query
			} else {
				left := updateSum(n.Left, idx, v)
				right := updateSum(n.Right, idx, v)
				n.Query = left + right
				return n.Query
			}
		} else {
			return n.Query
		}
	}

	obj.Root.Query = updateSum(obj.Root, index, value)
}

func (obj *SegmentTree) DisplayTree() {
	var display func(*Node)

	root := obj.Root
	display = func(root *Node) {
		if root != nil {
			display(root.Left)
			fmt.Printf("Sum->%d Interval->%d-%d\n", root.Query, root.Interval.X, root.Interval.Y)
			display(root.Right)
		}
	}

	display(root)
}
